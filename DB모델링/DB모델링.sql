﻿
# 사용자 
use sys;

# table 삭제 
# filter 에서 user까지 역순으로 drop 테이블 하면 됩니다. 
drop table user;
drop table study;
drop table reservation;
drop table room_info;
drop table review;
drop table filter;
drop table System_info; 

# 현재 테이블 순서는 DB모델링.xlsx와 같은 순서로 작성되어 있습니다. 

# User Table 생성
 
CREATE TABLE user (
  `Uid` CHAR(12) NOT NULL,
  `Upass` CHAR(100) NOT NULL,
  `Uname` CHAR(10) NOT NULL,
  `Uphone` CHAR(13) NOT NULL,
  `Uage` INT(2) NOT NULL,
  `Ujob` CHAR(15) NOT NULL,
  `Uact` TINYINT(1) NOT NULL,
  `Uemail` CHAR(50) NOT NULL,
  `Ugrade` TINYINT(1) NOT NULL,
  PRIMARY KEY (`Uid`));


# User Table sample data

 # buyer
INSERT INTO user VALUES('1', '1234' , '이상우', '010-1234-5678',25, '학생' , '0' ,'chjcmy@gamil.com',1);
 # seller
INSERT INTO user VALUES('2', '1234' , '박원진', '010-2345-6789',25, '학생' , '0' ,'chjcmy@gamil.com',2);
 # admin
INSERT INTO user VALUES('3', '1234' , '최성현', '010-3456-7890',25, '학생' , '0' ,'chjcmy@gamil.com',3); 



# Study Table 생성

CREATE TABLE study (
  `Sid` INT(12) NOT NULL  AUTO_INCREMENT,
  `Sname` char(20) NOT NULL,
  `Uid` char(12) NOT NULL, # 스터디카페 주인
  `Slocation` char(15),
  `Sstart` int NOT NULL,
  `Send` int NOT NULL,
  `Sbody` CHAR(200) NULL,
  `Sfile_1` CHAR(200) NULL, # 16mb 까지 첨부 가능 
  `Sfile_2` CHAR(200)NULL,
  `Sfile_3` CHAR(200) NULL,
  `Sprice` INT(6) NOT NULL,
  `Slat` INT(15) ,
  `Slong` INT(15) ,
  PRIMARY KEY (`Sid`),
  Foreign KEY (`Uid`) references user(`Uid`)
  on delete cascade);

 
# Study Table sample data

INSERT INTO study VALUES('1', '스터디1' ,'1' , '서울시 강남구', '09','22','샘플 스터디카페', null,null,null,2000,11111,11111 );
INSERT INTO study VALUES('2', '스터디2' , '2','서울시 서초구', '10','23','샘플 스터디카페2', null,null,null,2500,11111,11111 );
INSERT INTO study VALUES('3', '스터디3' , '3','서울시 노원구', '11','24','샘플 스터디카페3', null,null,null,3000,11111,11111 );


# reservation Table 생성

CREATE TABLE reservation (
  `Rid` INT(5) NOT NULL  AUTO_INCREMENT,
  `Sid` INT(12) NOT NULL,
  `Uid` CHAR(12) NOT NULL,
  `Rtable` INT(3) NULL,
  `Rdate` DATE NOT NULL,
  `Rstart` int NOT NULL,
  `Rend` int NOT NULL,
  `Rprice` INT(6) NOT NULL,
  `Rcount` INT(2) NOT NULL,
  `Rpurpose` CHAR(20) NULL,
  `Rprint` CHAR(200) NULL,
  PRIMARY KEY (`RID`),
  FOREIGN KEY (`Uid`) REFERENCES user (`Uid`),
  FOREIGN KEY (`Sid`) REFERENCES study (`Sid`)
   on delete cascade);

   INSERT INTO reservation VALUES(1,1,1,1,'2019-09-30','12','14',2000,2,'공부',1);
    INSERT INTO reservation VALUES(2,2,2,2,'2019-10-01','16','18',3000,4,'공부2',0);
    INSERT INTO reservation VALUES(3,3,3,3,'2019-10-02','18','20',4000,6,'공부3',1);


# Romm_info Table 생성

CREATE TABLE room_info(
`Room_id` INT(12) NOT NULL AUTO_INCREMENT,
`Sid` INT(12) NOT NULL , 
`Rtable` INT(3) NOT NULL ,
`Room_count` INT(2) NOT NULL , 
  PRIMARY KEY (`Room_id`),
  FOREIGN KEY (`Sid`) REFERENCES study (`Sid`)
  on delete cascade);  

#Room_info Table sample data

INSERT INTO room_info VALUES(1, 1, 1, 1 );
INSERT INTO room_info VALUES(2, 2, 1, 2 );
INSERT INTO room_info VALUES(3, 3, 2, 3 );

# Review Table 생성 

CREATE TABLE review(
`Review_id` INT(20) ,
`Sid` INT(12) NOT NULL,
`Uid` CHAR(12) NOT NULL Unique, 
`Rbody` CHAR(150),
  PRIMARY KEY (`Review_id`),
  FOREIGN KEY (`Uid`) REFERENCES user (`Uid`) on delete cascade,
  FOREIGN KEY (`Sid`) REFERENCES study (`Sid`) on delete cascade);  
  
  # Review Table sample data

  INSERT INTO review VALUES(1, 1, 1, '예시를 위한 리뷰입니다 !' );
  INSERT INTO review VALUES(2, 2, 2, '예시를 위한 리뷰입니다 !!' );
  INSERT INTO review VALUES(3, 3, 3, '예시를 위한 리뷰입니다 !!!' );
  
# Filter Table 생성 

CREATE TABLE filter(
`Fid` INT(12) NOT NULL,
`Fsearch` CHAR(12) , 
`Location` char(15),
`Fcount` INT(2),
`Fdate` date,
`Fprinter` boolean,
  PRIMARY KEY (`Fid`));  
  
  # Filter Table sample data
  
   INSERT INTO filter VALUES(1, '서울시 강남구 ', 1, 2, '2019-09-30',0);
   INSERT INTO filter VALUES(2, '서울시 서초구', 2, 4, '2019-10-01',0);
   INSERT INTO filter VALUES(3, '서울시 노원구', 3, 6, '2019-10-02',1);
   
  
  
# System_info Table 생성 

CREATE TABLE System_info(
`system_id` INT(2) NOT NULL,
`system_software` CHAR(20) NOT NULL Unique, 
`system_version` CHAR(10) NOT NULL ,
`system_body` CHAR(50) NULL,
  PRIMARY KEY (`system_id`));  
  
   # system_info Table sample data
   
    INSERT INTO System_info VALUES(1, 'java', '11.0.1','2019-09-30일 업데이트함');
   INSERT INTO System_info VALUES(2, 'mysql', '12.1.1','2019-09-30일 업데이트함');
   INSERT INTO System_info VALUES(3, 'tomcat', '6.0.2','2019-09-30일 업데이트함');
    
    
#  ---------------10월 4일 16:51 수정사항 -----------------

# study 테이블에 user id 를 참조키로 사용함 (스터디 주인을 구분하여 스터디 생성 권한을 부여하기 위함)
# 지역은 문자열로 변환하였으며, PK 의 조건은 만족하지 않아 FK로는 사용하지 못함 
# Filter 와 Reservation 의 지역이 어긋나지 않도록 유의 할 것 

#------------------------------------------------------
# 스터디 카페 조회 (이상우)
# 스터디 카페 이름 / 지역 / 시작 시간 / 마감 시간 / s_file_1 

# 스터디 카페 메인 조회
select Sid,Sname,Uid,Slocation,Sstart,Send,Sfile_1
from study
where Slocation like '%서울시%';

# 스터디 카페 상세 조회 
select Sname,Slocation,Sstart,Send,Sbody,Sfile_1,Sfile_2,Sfile_3,Sprice
from study
where Sid= 1;

# 스터디 카페 수정
select *
from study;

Delete from study
where Uid = 1 
and Sid = 1; 

