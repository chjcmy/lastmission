<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html>
<html>

<head>
<title>Study Cafe</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<%@ include file="/views/cssLoader.jsp"%>
</head>

<body>
	<%@ include file="/views/header.jsp"%>
	<div class="ftco-section-search">
		<div class="container">
			<div class="tab-content py-5" id="v-pills-tabContent">
				<div class="tab-pane fade show active" id="v-pills-home" role="tabpanel" aria-labelledby="v-pills-home-tab">
					<div class="block-17">
						<form action="${pageContext.request.contextPath}/cafe/cafeList.do" class="d-block d-lg-flex">
							<div class="fields d-block d-lg-flex">
								<input type="text" name="location" class="form-control" style="width: 80%" placeholder="Search Location">
							</div>
							<input type="submit" class="search-submit btn btn-primary" value="Find Cafe">
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
	<section class="ftco-section bg-light">
		<div class="container">
			<div class="row justify-content-center mb-5 pb-5">
				<div class="col-md-7 text-center heading-section ftco-animate">
					<h2>Study Reservation Services</h2>
				</div>
			</div>
			<div class="row">
				
				<div class="col-md-6 col-lg-3 d-flex align-self-stretch ftco-animate">
					<div class="media block-6 services d-block text-center">
						<div class="d-flex justify-content-center">
							<div class="icon d-flex justify-content-center mb-3">
								<span class="align-self-center flaticon-around"></span>
							</div>
						</div>
						<div class="media-body p-2">
							<h3 class="heading">스터디카페 플랫폼 통합</h3>
							<p>Study for you에서는  어떤 스터디카페 체인점이든 관계없이 모든 스터디의 정보 조회와   예약이 가능합니다 ! </p>
						</div>
					</div>
				</div>
				<div class="col-md-6 col-lg-3 d-flex align-self-stretch ftco-animate">
					<div class="media block-6 services d-block text-center">
						<div class="d-flex justify-content-center">
							<div class="icon d-flex justify-content-center mb-3">
								<span class="align-self-center flaticon-compass"></span>
							</div>
						</div>
						<div class="media-body p-2">
							<h3 class="heading">스터디 카페 창업 가이드</h3>
							<p>스터디카페 창업 시 별도의 어플을 개발하지 않아도 플랫폼을 통해 손쉽게 스터디카페를 등록하고, 예약 시스템을 구축할 수 있습니다.</p>
						</div>
					</div>
				</div>

				<div class="col-md-6 col-lg-3 d-flex align-self-stretch ftco-animate">
					<div class="media block-6 services d-block text-center">
						<div class="d-flex justify-content-center">
							<div class="icon d-flex justify-content-center mb-3">
								<span class="align-self-center flaticon-map-of-roads"></span>
							</div>
						</div>
						<div class="media-body p-2">
							<h3 class="heading">위치 정보 기반</h3>
							<p>각 스터디카페의 위도/경도 정보를 유지하여, 사용자가 손쉽게 위치를 확인 가능하며 지역명 기준으로 손쉬운 검색이 가능합니다.</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<section class="ftco-section">
    <div class="container-fluid">
      <div class="row no-gutters justify-content-center mb-5 pb-5 ftco-animate">
        <div class="col-md-7 text-center heading-section">
          <h2>협약 스터디카페 체인점</h2>
        </div>
      </div>
      <div class="row no-gutters">
        <div class="col-md-6 col-lg-3 ftco-animate">
          <a href="#" class="block-5" style="background-image: url('${pageContext.request.contextPath}/images/cafe (1).png');">

        
            <div class="text">
              <h3 class="heading">Study Bean</h3>
              <p class="star-rate"><span class="icon-star"></span><span class="icon-star"></span><span class="icon-star"></span><span class="icon-star"></span><span>1,000 reviews</span></p>
            </div>
          </a>
        </div>
        <div class="col-md-6 col-lg-3 ftco-animate">
                    <a href="#" class="block-5" style="background-image: url('${pageContext.request.contextPath}/images/cafe (2).png');">
            <div class="text">	
              <h3 class="heading">랭 스터디카페</h3>
              <p class="star-rate"><span class="icon-star"></span><span class="icon-star"></span><span class="icon-star"></span><span class="icon-star"></span><span class="icon-star"></span> <span>5,000 reviews</span></p>
            </div>
          </a>
        </div>
        <div class="col-md-6 col-lg-3 ftco-animate">
                    <a href="#" class="block-5" style="background-image: url('${pageContext.request.contextPath}/images/cafe (3).png');">
            <div class="text">
              <h3 class="heading">시작 스터디카페</h3>
              <p class="star-rate"><span class="icon-star"></span><span class="icon-star"></span><span class="icon-star"></span><span>500 reviews</span></p>
            </div>
          </a>
        </div>
        <div class="col-md-6 col-lg-3 ftco-animate">
                    <a href="#" class="block-5" style="background-image: url('${pageContext.request.contextPath}/images/cafe (4).png');">
            <div class="text">
              <h3 class="heading">초심 스터디카페</h3>
              <p class="star-rate"><span class="icon-star"></span><span class="icon-star"></span><span class="icon-star"></span><span class="icon-star"></span><span class="icon-star-half-full"></span> <span>3,750 reviews</span></p>
            </div>
          </a>
        </div>
        <div class="col-md-6 col-lg-3 ftco-animate">
                    <a href="#" class="block-5" style="background-image: url('${pageContext.request.contextPath}/images/cafe (5).png');">
            <div class="text">
              <h3 class="heading">Groo 스터디 센터</h3>
              <p class="star-rate"><span class="icon-star"></span></span><span class="icon-star"></span><span class="icon-star"></span><span class="icon-star-half-full"></span> <span>3,500 reviews</span></p>
            </div>
          </a>
        </div>
        <div class="col-md-6 col-lg-3 ftco-animate">
                    <a href="#" class="block-5" style="background-image: url('${pageContext.request.contextPath}/images/cafe (6).png');">
            <div class="text">
              <h3 class="heading">Ageis StudyCafe</h3>
              <p class="star-rate"><span class="icon-star"></span><span class="icon-star"></span><span class="icon-star-half-full"></span> <span>1,500 reviews</span></p>
            </div>
          </a>
        </div>
        <div class="col-md-6 col-lg-3 ftco-animate">
                   <a href="#" class="block-5" style="background-image: url('${pageContext.request.contextPath}/images/cafe (7).png');">
            <div class="text">
              <h3 class="heading">르하임 스터디카페</h3>
              <p class="star-rate"><span class="icon-star"></span><span class="icon-star"></span><span class="icon-star"></span><span class="icon-star"></span><span class="icon-star-half-full"></span> <span>4,500 reviews</span></p>
            </div>
          </a>
        </div>
        <div class="col-md-6 col-lg-3 ftco-animate">
                   <a href="#" class="block-5" style="background-image: url('${pageContext.request.contextPath}/images/cafe (8).png');">
            <div class="text">
              <h3 class="heading">달빛 스터디카페</h3>
              <p class="star-rate"><span class="icon-star"></span><span class="icon-star"></span><span class="icon-star"></span><span class="icon-star"></span><span class="icon-star"></span> <span>5,000 reviews</span></p>
            </div>
          </a>
        </div>
      </div>
    </div>
  </section>
	<!-- 불필요 -->
	<!--
 

  <section class="ftco-section bg-light">
    <div class="container">
      <div class="row justify-content-center mb-5 pb-5">
        <div class="col-md-7 text-center heading-section ftco-animate">
          <h2>Our Services</h2>
        </div>
      </div>
      <div class="row">
        <div class="col-md-6 col-lg-3 d-flex align-self-stretch ftco-animate">
          <div class="media block-6 services d-block text-center">
            <div class="d-flex justify-content-center">
              <div class="icon d-flex justify-content-center mb-3"><span class="align-self-center flaticon-sailboat"></span></div>
            </div>
            <div class="media-body p-2">
              <h3 class="heading">Special Activities</h3>
              <p>Even the all-powerful Pointing has no control about the blind texts it is an almost unorthographic.</p>
            </div>
          </div>
        </div>
        <div class="col-md-6 col-lg-3 d-flex align-self-stretch ftco-animate">
          <div class="media block-6 services d-block text-center">
            <div class="d-flex justify-content-center">
              <div class="icon d-flex justify-content-center mb-3"><span class="align-self-center flaticon-around"></span></div>
            </div>
            <div class="media-body p-2">
              <h3 class="heading">Travel Arrangements</h3>
              <p>Even the all-powerful Pointing has no control about the blind texts it is an almost unorthographic.</p>
            </div>
          </div>
        </div>
        <div class="col-md-6 col-lg-3 d-flex align-self-stretch ftco-animate">
          <div class="media block-6 services d-block text-center">
            <div class="d-flex justify-content-center">
              <div class="icon d-flex justify-content-center mb-3"><span class="align-self-center flaticon-compass"></span></div>
            </div>
            <div class="media-body p-2">
              <h3 class="heading">Private Guide</h3>
              <p>Even the all-powerful Pointing has no control about the blind texts it is an almost unorthographic.</p>
            </div>
          </div>
        </div>

        <div class="col-md-6 col-lg-3 d-flex align-self-stretch ftco-animate">
          <div class="media block-6 services d-block text-center">
            <div class="d-flex justify-content-center">
              <div class="icon d-flex justify-content-center mb-3"><span class="align-self-center flaticon-map-of-roads"></span></div>
            </div>
            <div class="media-body p-2">
              <h3 class="heading">Location Manager</h3>
              <p>Even the all-powerful Pointing has no control about the blind texts it is an almost unorthographic.</p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>

  

  

  <section class="ftco-section">
    <div class="container-fluid">
      <div class="row mb-5 pb-5 no-gutters">
        <div class="col-lg-4 bg-light p-3 p-md-5 d-flex align-items-center heading-section ftco-animate">
          <div>
            <h2 class="mb-5 pb-3">Want to get our hottest travel deals top tips and advice? Subscribe us now!</h2>
            <form action="#" class="subscribe-form">
              <div class="form-group">
                <span class="icon icon-paper-plane"></span>
                <input type="text" class="form-control" placeholder="Enter your email address">
              </div>
            </form>
          </div>
        </div>
        <div class="col-lg-8 p-2 pl-md-5 heading-section">
          <h2 class="mb-5 p-2 pb-3 ftco-animate">Most Recommended Hotels</h2>
          <div class="row no-gutters d-flex">
            <div class="col-md-4 ftco-animate">
              <a href="#" class="block-5" style="background-image: url('images/hotel-1.jpg');">
                <div class="text">
                  <span class="price">$29/night</span>
                  <h3 class="heading">Luxe Hotel</h3>
                  <div class="post-meta">
                    <span>Ameeru Ahmed Magu Male’, Maldives</span>
                  </div>
                  <p class="star-rate"><span class="icon-star"></span><span class="icon-star"></span><span class="icon-star"></span><span class="icon-star"></span><span class="icon-star-half-full"></span> <span>500 reviews</span></p>
                </div>
              </a>
            </div>
            <div class="col-md-4 ftco-animate">
              <a href="#" class="block-5" style="background-image: url('images/hotel-2.jpg');">
                <div class="text">
                  <span class="price">$29/night</span>
                  <h3 class="heading">Deluxe Hotel</h3>
                  <div class="post-meta">
                    <span>Ameeru Ahmed Magu Male’, Maldives</span>
                  </div>
                  <p class="star-rate"><span class="icon-star"></span><span class="icon-star"></span><span class="icon-star"></span><span class="icon-star"></span><span class="icon-star-half-full"></span> <span>500 reviews</span></p>
                </div>
              </a>
            </div>
            <div class="col-md-4 ftco-animate">
              <a href="#" class="block-5" style="background-image: url('images/hotel-3.jpg');">
                <div class="text">
                  <span class="price">$29/night</span>
                  <h3 class="heading">Deluxe Hotel</h3>
                  <div class="post-meta">
                    <span>Ameeru Ahmed Magu Male’, Maldives</span>
                  </div>
                  <p class="star-rate"><span class="icon-star"></span><span class="icon-star"></span><span class="icon-star"></span><span class="icon-star"></span><span class="icon-star-half-full"></span> <span>500 reviews</span></p>
                </div>
              </a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>

  <section class="ftco-section bg-light">
    <div class="container">
      <div class="row justify-content-center mb-5 pb-5">
        <div class="col-md-7 text-center heading-section ftco-animate">
          <h2>Our Blog</h2>
        </div>
      </div>
      <div class="row ftco-animate">
        <div class="carousel1 owl-carousel ftco-owl">
          <div class="item">
            <div class="blog-entry">
              <a href="blog-single.html" class="block-20" style="background-image: url('images/image_5.jpg');">
              </a>
              <div class="text p-4">
                <div class="meta">
                  <div><a href="#">July 7, 2018</a></div>
                  <div><a href="#">Admin</a></div>
                </div>
                <h3 class="heading"><a href="#">Even the all-powerful Pointing has no control about the blind texts</a></h3>
                <p class="clearfix">
                  <a href="#" class="float-left">Read more</a>
                  <a href="#" class="float-right meta-chat"><span class="icon-chat"></span> 3</a>
                </p>
              </div>
            </div>
          </div>
          <div class="item">
            <div class="blog-entry" data-aos-delay="100">
              <a href="blog-single.html" class="block-20" style="background-image: url('images/image_6.jpg');">
              </a>
              <div class="text p-4">
                <div class="meta">
                  <div><a href="#">July 7, 2018</a></div>
                  <div><a href="#">Admin</a></div>
                </div>
                <h3 class="heading"><a href="#">Even the all-powerful Pointing has no control about the blind texts</a></h3>
                <p class="clearfix">
                  <a href="#" class="float-left">Read more</a>
                  <a href="#" class="float-right meta-chat"><span class="icon-chat"></span> 3</a>
                </p>
              </div>
            </div>
          </div>
          <div class="item">
            <div class="blog-entry" data-aos-delay="200">
              <a href="blog-single.html" class="block-20" style="background-image: url('images/image_7.jpg');">
              </a>
              <div class="text p-4">
                <div class="meta">
                  <div><a href="#">July 7, 2018</a></div>
                  <div><a href="#">Admin</a></div>
                </div>
                <h3 class="heading"><a href="#">Even the all-powerful Pointing has no control about the blind texts</a></h3>
                <p class="clearfix">
                  <a href="#" class="float-left">Read more</a>
                  <a href="#" class="float-right meta-chat"><span class="icon-chat"></span> 3</a>
                </p>
              </div>
            </div>
          </div>
          <div class="item">
            <div class="blog-entry" data-aos-delay="200">
              <a href="blog-single.html" class="block-20" style="background-image: url('images/image_8.jpg');">
              </a>
              <div class="text p-4">
                <div class="meta">
                  <div><a href="#">July 7, 2018</a></div>
                  <div><a href="#">Admin</a></div>
                </div>
                <h3 class="heading"><a href="#">Even the all-powerful Pointing has no control about the blind texts</a></h3>
                <p class="clearfix">
                  <a href="#" class="float-left">Read more</a>
                  <a href="#" class="float-right meta-chat"><span class="icon-chat"></span> 3</a>
                </p>
              </div>
            </div>
          </div>
          <div class="item">
            <div class="blog-entry" data-aos-delay="200">
              <a href="blog-single.html" class="block-20" style="background-image: url('images/image_9.jpg');">
              </a>
              <div class="text p-4">
                <div class="meta">
                  <div><a href="#">July 7, 2018</a></div>
                  <div><a href="#">Admin</a></div>
                </div>
                <h3 class="heading"><a href="#">Even the all-powerful Pointing has no control about the blind texts</a></h3>
                <p class="clearfix">
                  <a href="#" class="float-left">Read more</a>
                  <a href="#" class="float-right meta-chat"><span class="icon-chat"></span> 3</a>
                </p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
-->


	<%@ include file="footer.jsp"%>
	<!-- loader -->
	<div id="ftco-loader" class="show fullscreen">
		<svg class="circular" width="48px" height="48px">
      <circle class="path-bg" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke="#eeeeee" />
      <circle class="path" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke-miterlimit="10" stroke="#F96D00" /></svg>
	</div>

	<%@ include file="jsLoader.jsp"%>
</body>

</html>
