<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">

<head>
<title>Voyage - Free Bootstrap 4 Template by Colorlib</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<%@ include file="/views/cssLoader.jsp"%>
</head>

<style>
/* Always set the map height explicitly to define the size of the div
       * element that contains the map. */
#map {
	height: 100%;
}
/* Optional: Makes the sample page fill the window. */
html, body {
	height: 100%;
	margin: 0;
	padding: 0;
}
</style>

<script type="text/javascript" src="//dapi.kakao.com/v2/maps/sdk.js?appkey=5cd7a50565e37231c8c76a0f43888843"></script>
<body>

	<%@ include file="/views/header.jsp"%>
	<!-- END comment-list -->

	<section class="ftco-section">
		<div class="container">
			<div class="row">
				<div class="col-md-8 ftco-animate">
					<h2 class="mb-3">${cafeInfo.cafeName}</h2>
					<p>${cafeInfo.cafeBody}</p>


					<!--슬라이더-->
					<section class="home-slider owl-carousel">
						<div class="item">
							<div class="slider-item2" style="background-image: url('${cafeInfo.cafePicture1}');"></div>
						</div>
						<div class="item">
							<div class="slider-item2" style="background-image: url('${cafeInfo.cafePicture3}');"></div>
						</div>
					</section>
					<div id="map" style="width: 700px; height: 400px;"></div>
					<!--end 슬라이더-->
					<c:choose>
						<c:when test="${null ne sessionScope.userID}">
							<div class="pt-3 mt-1">
								<button type="button" class="search-submit btn btn-outline-success h-25, w-100" onclick="location.href='${pageContext.request.contextPath}/res/res.do?cafeId=${cafeInfo.cafeId}'">예약</button>
							</div>
						</c:when>
					</c:choose>

				</div>
				<!-- .col-md-8 -->
				<div class="col-md-4 sidebar">
					<div class="sidebar-box ftco-animate">
						<div class="search-tours bg-light p-4">
							<h3>카페 찾기</h3>
							<form action="${pageContext.request.contextPath}/cafe/cafeList.do">
								<div class="fields">
									<div class="row flex-column">

										<div class="textfield-search col-sm-12 group mb-3">
											<input type="text" name="location" class="form-control" placeholder="지역">
										</div>

										<div class="col-sm-12 group mb-3">
											<input type="submit" class="search-submit btn btn-primary" value="find Cafe">
										</div>
									</div>
								</div>
							</form>
						</div>
					</div>

				</div>

			</div>


		</div>
	</section>
	<!-- .section -->


	
	<%@ include file="footer.jsp"%>
	<!-- loader -->

	<%@ include file="jsLoader.jsp"%>
	<script>
		var mapContainer = document.getElementById('map'), 
		mapOption = { 
		    center: new kakao.maps.LatLng(parseFloat( ${cafeInfo.clat}), parseFloat(${cafeInfo.clong})), 
		    level: 4 
		};
		var map = new kakao.maps.Map(mapContainer, mapOption); 
		
		var markers = [];
		
		addMarker(new kakao.maps.LatLng(parseFloat( ${cafeInfo.clat}), parseFloat(${cafeInfo.clong})));
			
			function addMarker(position) {
			    
			    var marker = new kakao.maps.Marker({
			        position: position
			    });
			
			    marker.setMap(map);
			}
	</script>
</body>

</html>