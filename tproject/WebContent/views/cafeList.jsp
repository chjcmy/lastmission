<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="UTF-8">
<head>
<title>Voyage - Free Bootstrap 4 Template by Colorlib</title>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script type="text/javascript">
	function page(idx) {
		var pagenum = idx;
		var contentnum = $("#contentnum option:selected").val();
		location.href = "${pageContext.request.contextPath}/cafe/cafeList.do?pagenum="
				+ pagenum + "&contentnum=" + contentnum;
	}
</script>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<%@ include file="cssLoader.jsp"%>
</head>
<body>
	<%@ include file="header.jsp"%>
	<select name="contentnum" id="contentnum">
		<!-- 10개씩 , 20개씩 , 30개씩 사용자가 원하는 만큼 게시글을 볼 수 있도록 작성했으나 , 추후 구현 예정입니다 -->
		<option value="10">10</option>
		<option value="20">20</option>
		<option value="30">30</option>
	</select>



	<section class="ftco-section">
		<div class="container">
			<div class="row">
				<div class="col-lg-8">
					<div class="search-tours bg-light p-4">
						<h3>카페 찾기</h3>
						<form action="${pageContext.request.contextPath}/cafe/cafeList.do">
							<div class="fields">
								<div class="row flex-column">

									<div class="textfield-search col-sm-12 group mb-3">
										<input type="text" name="location" class="form-control" placeholder="지역">
									</div>

									<div class="col-sm-12 group mb-3">
										<input type="submit" class="search-submit btn btn-primary" value="find Cafe">
									</div>
								</div>
							</div>
						</form>

					</div>
					<div class="row">
						<c:forEach var="cafeInfo" items="${cafeList}">
							<div class="col-md-6 col-lg-6 mb-4 ftco-animate">
								<a href="${pageContext.request.contextPath}/cafe/cafeDetail.do?cafeId=${cafeInfo.cafeId}" class="block-5" style="background-image: url('${cafeInfo.cafePicture2}');">
									<div class="text">
										<span class="price">${cafeInfo.cafeName}</span>
										<h3 class="heading">${cafeInfo.cafePrice}</h3>
										<div class="post-meta">
											<span>영업시간: ${cafeInfo.cafeStart}:00 ~ ${cafeInfo.cafeEnd}:00</span>
										</div>
									</div>
								</a>
							</div>
						</c:forEach>
					</div>




					<div class="col text-center">
						<div class="block-27">
							<ul>
								<!-- 왼쪽 화살표 -->
								<c:if test="${page.prev}">
									<li><a href="javascript:page(${page.getStartPage()-1});">&laquo;</a></li>
								</c:if>
								<!-- 페이지 숫자 표시 -->
								<c:forEach begin="${page.getStartPage()}" end="${page.getEndPage()}" var="idx">
									<li><a href="javascript:page(${idx});"> ${idx}</a></li>
								</c:forEach>

								<!-- 오른쪽 화살표 -->
								<c:if test="${page.next}">
									<li><a href="javascript:page(${page.getEndPage()+1});">&raquo;</a></li>
								</c:if>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>

	</section>

	<%@ include file="footer.jsp"%>


	<!-- loader -->
	<div id="ftco-loader" class="show fullscreen">
		<svg class="circular" width="48px" height="48px">
			<circle class="path-bg" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke="#eeeeee" />
			<circle class="path" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke-miterlimit="10" stroke="#F96D00" /></svg>
	</div>


	<%@ include file="jsLoader.jsp"%>
</body>
</html>
