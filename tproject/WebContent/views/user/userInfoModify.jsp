<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<%@ include file="/views/cssLoader.jsp"%>
</head>
<body>
	<%@ include file="/views/header.jsp"%>
	<section class="ftco-section">
		<div class="container">
			<form name="info" action="${pageContext.request.contextPath}/user/update.do" method="POST" class="search-destination">
				<div class="row">
					<div class="col-md-12">
						<h2>정보수정</h2>
						<table class="table table-boardered">
							<tr>
								<th>아이디</th>
								<td><input type="text" name="UserID" class="form-control"
									value="${sessionScope.userID}" readonly="readonly"></td>
							</tr>
							<tr>
								<th>패스워드</th>
								<td><input type="password" name="UserPW"
									class="form-control" placeholder="회원정보 수정 완료시 패스워드 재입력" readonly="readonly"></td>
							</tr>
							<tr>
								<th>패스워드 확인</th>
								<td><input type="password" class="form-control"
									name="UserPW2" readonly="readonly"></td>
							</tr>

							<tr>
								<th>이름</th>
								<td><input type="text" name="UserName" id="userName"
									class="form-control" value="${sessionScope.userName}"  readonly="readonly">
							</tr>
							<tr>
								<th>이메일</th>
								<td><input type="text" name="UserMail" class="form-control"
									value="${sessionScope.userMail}" readonly="readonly"></td>
							</tr>

							<tr>
								<th>전화번호</th>
								<td><input type="text" name="UserTel" class="form-control"
									value="${sessionScope.userTel}" readonly="readonly"></td>
							</tr>
							<!-- 
							<tr>
								<th>당신의 관심분야</th>
								<td><input type="checkbox" name="hobby" value="캠핑">캠핑 &nbsp;&nbsp; <input type="checkbox" name="hobby" value="등산">등산 &nbsp;&nbsp; <input type="checkbox" name="hobby" value="영화">영화 &nbsp;&nbsp; <input type="checkbox" name="hobby" value="독서">독서 &nbsp;&nbsp;</td>
							</tr>
							-->
							<tr>
								<th>당신의 직업은</th>
								<td><select name="UserJob" class="form-control">
										<option value="학생">학생</option>
										<option value="취준생">취준생</option>
										<option value="회사원">회사원</option>
								</select></td>
							</tr>


							<tr>
								<th>당신의 연력은</th>
								<td><input type="radio" name="UserAge" value="10"
									readonly="readonly">10대 &nbsp;&nbsp; <input
									type="radio" name="UserAge" value="20" readonly="readonly">20대
									&nbsp;&nbsp; <input type="radio" name="UserAge" value="30"
									readonly="readonly">30대 &nbsp;&nbsp; <input
									type="radio" name="UserAge" value="40" readonly="readonly">40대
									&nbsp;&nbsp;</td>
							</tr>
							<th>등급 확인</th>
								<td><select name="UserGrade" value= "${sessionScope.userGrade}" readonly class="form-control">
										<option value="1">사용자</option>
										<option value="2">카페 점주</option>
										<option value="3">관리자</option>
								</select></td>
						</table>
						</div>
						</div>

							<tr align="center">
								<td colspan="2">
								<input type="button" value="회원정보 수정하기" id="modifyInfo" onclick="modifyClick()" class="btn btn-success py-3 px-5">
								<input type="submit" value="회원정보 수정완료" id="modifyFinish" class="btn btn-success py-3 px-5" hidden = "true">
								<input type="button" value="회원탈퇴하기" id="deleteUser" data-toggle="modal" data-target="#deleteModal" class="btn btn-danger pull-right" hidden="true"> 
								<input type="reset" class="btn btn-warning py-3 px-5" value="취소"></td>
							</tr>

		
		</form>
		</div>
	</section>

	<!-- 회원탈퇴 버튼누르면 출력될 모달 -->
	<div class="modal fade" id="deleteModal" tabindex="1" role="dialog"
		aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog">
			<form
				action="${pageContext.request.contextPath}/user/deleteConfirm.do"
				method="post" class="search-destination">
				<div class="modal-content">
					<div class="modal-header ">
						<h4 class="modal-title" id="deleteModalLabel">회원탈퇴</h4>
					</div>
					<div class="modal-body">
						<div class="form-field">
							<div class="icon">
								<span class="icon-compass"></span>&nbsp; 패스워드 재입력
							</div>
							<input type="password" name="inputpassword" class="form-control"
								placeholder="패스워드를 입력해주세요">
						</div>
						<br>

					</div>
					<div class="modal-footer">

						<button type="button" class="btn btn-default" data-dismiss="modal">창
							닫기</button>

						<button type="submit" class="btn btn-primary">회원탈퇴</button>

					</div>
				</div>
			</form>
		</div>
	</div>

	<%@ include file="/views/footer.jsp"%>
	<!-- loader -->
	<div id="ftco-loader" class="show fullscreen">
		<svg class="circular" width="48px" height="48px">
      <circle class="path-bg" cx="24" cy="24" r="22" fill="none"
				stroke-width="4" stroke="#eeeeee" />
      <circle class="path" cx="24" cy="24" r="22" fill="none"
				stroke-width="4" stroke-miterlimit="10" stroke="#F96D00" /></svg>
	</div>
	<script>
		function modifyClick() {
			document.info.UserName.readOnly = false;
			document.info.UserPW.readOnly = false;
			document.info.UserPW2.readOnly = false;
			document.info.UserMail.readOnly = false;
			document.info.UserTel.readOnly = false;
			document.info.UserJob.readOnly = false;
			document.info.UserAge.readOnly = false;
			document.info.modifyFinish.hidden = false;
			document.info.deleteUser.hidden = false;
			document.info.modifyInfo.hidden = true;

		}
		</script>
	<%@ include file="/views/jsLoader.jsp"%>
</body>
</html>