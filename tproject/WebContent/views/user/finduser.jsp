<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<%@ include file="/views/cssLoader.jsp"%>
</head>
<script type="text/javascript"
	src="http://code.jquery.com/jquery-1.11.3.js"></script>
<script type="text/javascript">
   $(document)
         .ready(
               function() {

                  $('#btn2')
                        .on(
                              'click',
                              function() {
                                 $
                                       .ajax({
                                          url : "/tproject/user/findid.do",
                                          contentType : "application/x-www-form-urlencoded; charset=UTF-8",
                                          type : "POST",
                                          data : $("#fid")
                                                .serialize(),
                                          success : function(data) {
                                             var data1 = decodeURIComponent(data);
                                             $('#result').text(
                                                   data1);
                                          },
                                          error : function() {
                                             alert("simpleWithObject err");
                                          }
                                       });
                              });
               })
</script>
<script type="text/javascript">
   $(document)
         .ready(
               function() {

                  $('#btn3')
                        .on(
                              'click',
                              function() {
                                 $
                                       .ajax({
                                          url : "/tproject/user/findpw.do",
                                          contentType : "application/x-www-form-urlencoded; charset=UTF-8",
                                          type : "POST",
                                          data : $("#fpw")
                                                .serialize(),
                                          success : function(data) {
                                             var data1 = decodeURIComponent(data);
                                             $('#result2').text(
                                                   data1);
                                          },
                                          error : function() {
                                             alert("simpleWithObject err");
                                          }
                                       });
                              });
               })
</script>
<body>
   <%@ include file="/views/header.jsp"%>
   <section class="ftco-section">
      <div class="container">
         <div class="row">
            <div class="col-md-12">
               <h2>아이디 찾기</h2>
               <form id="fid">
                  <table class="table table-boardered">
                     <tr>
                        <th>이름</th>

                        <td><input type="text" name="userName" id="userName"></td>

                     </tr>

                     <tr>
                        <th>이메일</th>
                        <td><input type="text" name="userMail" id="userMail">
                        </td>
                     </tr>
                  </table>
               </form>

            </div>
         </div>
         <tr align="center">
            <td colspan="2"><button class="btn btn-success py-3 px-5"
                  id="btn2">아이디찾기찾기</button> <input type="reset"
               class="btn btn-warning py-3 px-5" value="취소">
               <div id="result" style="font-size: 2em; color: green;"></div>
         </tr>

      </div>
   </section>
   <section class="ftco-section">
      <div class="container">
         <div class="row">
            <div class="col-md-12">
               <h2>비밀번호 찾기</h2>
               <form id="fpw">
                  <table class="table table-boardered">
                     <tr>
                        <th>아이디</th>

                        <td><input type="text" name="userID" id="userID"></td>

                     </tr>

                     <tr>
                        <th>이름</th>

                        <td><input type="text" name="userName" id="userName"></td>

                     </tr>

                     <tr>
                        <th>이메일</th>
                        <td><input type="text" name="userMail" id="userMail">
                           <div id="result" style="font-size: 2em; color: green;"></div></td>

                     </tr>




                  </table>
               </form>
               <tr align="center">
                  <td colspan="2"><button class="btn btn-success py-3 px-5"
                        id="btn3">비밀번호 찾기</button> <input type="reset"
                     class="btn btn-warning py-3 px-5" value="취소">
                     <div id="result2" style="font-size: 2em; color: green;"></div>
               </tr>
            </div>
         </div>

      </div>
   </section>
   <%@ include file="/views/footer.jsp"%>
   <!-- loader -->
   <div id="ftco-loader" class="show fullscreen">
      <svg class="circular" width="48px" height="48px">
      <circle class="path-bg" cx="24" cy="24" r="22" fill="none"
            stroke-width="4" stroke="#eeeeee" />
      <circle class="path" cx="24" cy="24" r="22" fill="none"
            stroke-width="4" stroke-miterlimit="10" stroke="#F96D00" /></svg>
   </div>


   <%@ include file="/views/jsLoader.jsp"%>
</body>
</html>