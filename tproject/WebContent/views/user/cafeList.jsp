<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<%@ include file="/views/cssLoader.jsp"%>
</head>
<body>
	<%@ include file="/views/header.jsp"%>
	<section class="ftco-section">

		<div class="container">
			<div class="row">
				<div style="z-index: 1020" class="col-md-12">
					<div class="list-group panel panel-success hidden">
						<div class="panel-heading list-group-item text-center hidden-xs">
							<h4>스터디카페 예약내역</h4>
						</div>
					</div>
					<div class="d-block d-lg-flex">
						<div class="check-out one-third one-third-1">
							<input type="text" id="start_date" class="form-control" placeholder="날짜 선택">
						</div>
						<input type="button" id="findRes" class="search-submit btn btn-primary" value="검색">
					</div>
				</div>
			</div>


			<div class="col-md-12">

				<div class="table-responsive">
					<table class="table table-striped table-bordered table-hover">
						<colgroup>
							<col width="*" />
							<col width="*" />
							<col width="*" />
							<col width="*" />
							<col width="*" />
							<col width="*" />
							<col width="*" />
							<col width="*" />
						</colgroup>
						<thead>
							<tr>
								<th class="text-center">기간</th>
								<th class="text-center">인원</th>
								<th class="text-center">가격</th>
								<th class="text-center">테이블 번호</th>
								<th class="text-center">첨부파일</th>
							</tr>
						</thead>
						<tbody>
							<c:choose>
								<c:when test="${empty userList}">
									<tr>
										<th colspan="9" class="text-center">예약 내역이 존재하지 않습니다.</th>
									</tr>
								</c:when>
								<c:otherwise>
									<c:forEach var="userTable" items="${userList}">
										<tr>
											<td class="text-center">${userTable.rDate}/ ${userTable.startTime}~${userTable.endTime}</td>
											<td class="text-center">${userTable.personNum}</td>
											<td class="text-center">${userTable.price}</td>
											<td class="text-center">${userTable.tablenum}</td>
											<c:choose>
												<c:when test="${userTable.file ne null}">
													<td><form action="${pageContext.request.contextPath}/res/down2.do">
															<input type=hidden name="file" value="${userTable.file}" style="width: 100%">
															<button type="submit" id="btnsubmit" style="width: 100%" style="width: 100%" class="btn btn-success">다운</button>
														</form></td>
												</c:when>
												<c:otherwise>
													<td class="text-center">없음</td>
												</c:otherwise>
											</c:choose>

										</tr>
									</c:forEach>
								</c:otherwise>
							</c:choose>
						</tbody>
					</table>
				</div>

			</div>
		</div>
	</section>

	<br>
	<script>
	//검색 했을때 작동 
			$('#findRes').click(function(){
				var date = $('#start_date');
				alert(date);
			});
	</script>

	<%@ include file="/views/footer.jsp"%>
	

	<%@ include file="/views/jsLoader.jsp"%>
</body>
</html>
