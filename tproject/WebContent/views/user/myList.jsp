<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<%@ include file="/views/cssLoader.jsp"%>
</head>
<body>
	<%@ include file="/views/header.jsp"%>
	<section class="ftco-section" fidden=false>

		<div class="container">
			<div class="row">

				<div style="z-index: 1020" class="col-md-12">
					<div class="list-group panel panel-success hidden">
						<div class="panel-heading list-group-item text-center hidden-xs">
							<h4>회원 예약내역</h4>
						</div>


					</div>
				</div>

				<div class="col-md-12">

					<div class="table-responsive">
						<table class="table table-striped table-bordered table-hover">
							<colgroup>
								<col width="*" />
								<col width="*" />
								<col width="*" />
								<col width="*" />
								<col width="*" />
								<col width="*" />
							</colgroup>
							<thead>
								<tr>
									<th class="text-center">카페이름</th>
									<th class="text-center">기간</th>
									<th class="text-center">인원</th>
									<th class="text-center">가격</th>
									<th class="text-center">테이블 번호</th>
									<th class="text-center">첨부파일</th>
									<th class="text-center">예약취소</th>
								</tr>
							</thead>
							<tbody>
								<c:choose>
									<c:when test="${empty userList}">
										<tr>
											<th colspan="9" class="text-center">예약 내역이 존재하지 않습니다.</th>
										</tr>
									</c:when>
									<c:otherwise>
										<c:forEach var="userTable" items="${userList}">
											<tr>
												<td class="text-center">${userTable.cafename}</td>
												<td class="text-center">${userTable.rDate}    /    ${userTable.startTime}~${userTable.endTime}</td>
												<td class="text-center">${userTable.personNum}</td>
												<td class="text-center">${userTable.price}</td>
												<td class="text-center">${userTable.tablenum}</td>
												<c:choose>
													<c:when test="${userTable.file ne null}">
														<td><form action="${pageContext.request.contextPath}/res/down.do">
																<input type=hidden name="file" value="${userTable.file}"style="width: 100%">
																<button type="submit" id="btnsubmit" style="width: 100%" class="btn btn-success">다운</button>
															</form></td>
													</c:when>
													<c:otherwise>
														<td class="text-center">없음</td>
													</c:otherwise>
												</c:choose>

												<td><form>
														<input type=submit value="예약 취소" style="width: 100%">
													</form></td>
											</tr>
										</c:forEach>
									</c:otherwise>
								</c:choose>
							</tbody>
						</table>
					</div>

				</div>
			</div>
		</div>
	</section>

	<br>
	<Script>
		<c:forEach var="plan" items="${plans}">
		$("#deletePlan${plan.planID}")
				.click(
						function() {

							var planID = $
							{
								plan.planID
							}
							;

							console.log(planID + "번 삭제요청!!");
							$
									.ajax({
										type : 'GET',
										url : "${pageContext.request.contextPath}/project/deletePlan.do",
										crossOrigin : false,
										data : {
											planID : planID
										},
										dataType : "json",
										contentType : "application/json; charset=UTF-8",
										success : function(data) {
											if (data == true) {
												swal.fire({
													type : 'success',
													title : '삭제되었습니다!',
													showConfirmButton : false,
													timer : 1500
												});

												location.href = "${pageContext.request.contextPath}/user/userinfo.do";
											} else {
												swal.fire({
													type : 'error',
													title : '에러가 발생하였습니다!',
													showConfirmButton : false,
													timer : 1500
												});

												location.href = "${pageContext.request.contextPath}/user/userinfo.do";
											}
										},
										failure : function(error) {
											swal.fire({
												type : 'error',
												title : '에러가 발생하였습니다!',
												showConfirmButton : false,
												timer : 1500
											});
											location.href = "${pageContext.request.contextPath}/user/userinfo.do";
										}
									});

						});

		</c:forEach>
		
	</Script>

	<%@ include file="/views/footer.jsp"%>
	<!-- loader -->
	<div id="ftco-loader" class="show fullscreen">
		<svg class="circular" width="48px" height="48px">
      <circle class="path-bg" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke="#eeeeee" />
      <circle class="path" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke-miterlimit="10" stroke="#F96D00" /></svg>
	</div>

	<%@ include file="/views/jsLoader.jsp"%>
</body>
</html>
