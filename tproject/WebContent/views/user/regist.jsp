<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<%@ include file="/views/cssLoader.jsp"%>
</head>
<body>
	<%@ include file="/views/header.jsp"%>
	<section class="ftco-section">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<h2>회원가입</h2>
					<form action="${pageContext.request.contextPath}/user/regist.do"
						method="post" class="search-destination">
						<table class="table table-boardered">

							<tr>
								<th>아이디</th>

								<td><input type="text" class="form-control" name="userID"
									placeholder="id를 넣으세요"></td>

							</tr>
							<tr>
								<th>패스워드</th>
								<td><input type="password" class="form-control"
									name="UserPW" placeholder="비밀번호는 영문만 넣어주세요"></td>
							</tr>

							<tr>
								<th>패스워드 확인</th>
								<td><input type="password" class="form-control"
									name="UserPW2"></td>
							</tr>

							<tr>
								<th>이름</th>
								<td><input type="name" class="form-control" name="UserName"></td>
							</tr>

							<tr>
								<th>전화번호</th>
								<td><input type="tel" class="form-control" name="UserTel"></td>
							</tr>

							<tr>
								<th>이메일</th>
								<td><input type="tel" class="form-control" name="UserMail"></td>
							</tr>
							<!-- 
							<tr>
								<th>당신의 관심분야</th>
								<td><input type="checkbox" name="hobby" value="캠핑">캠핑 &nbsp;&nbsp; <input type="checkbox" name="hobby" value="등산">등산 &nbsp;&nbsp; <input type="checkbox" name="hobby" value="영화">영화 &nbsp;&nbsp; <input type="checkbox" name="hobby" value="독서">독서 &nbsp;&nbsp;</td>
							</tr>
							-->
							<tr>
								<th>당신의 직업은</th>
								<td><select name="UserJob" class="form-control">
										<option value="학생">학생</option>
										<option value="취준생">취준생</option>
										<option value="회사원">회사원</option>
								</select></td>
							<tr>
								<th>당신의 연령은</th>
								<td><input type="radio" name="UserAge" value="10">10대
									&nbsp;&nbsp; <input type="radio" name="UserAge" value="20">20대
									&nbsp;&nbsp; <input type="radio" name="UserAge" value="30">30대
									&nbsp;&nbsp; <input type="radio" name="UserAge" value="40">40대
									&nbsp;&nbsp;</td>
							</tr>
							<tr>
							<th>등급 확인</th>
								<td><select name="UserGrade" class="form-control">
										<option value="1">사용자</option>
										<option value="2">카페 점주</option>
								</select></td>

							<tr align="center">
								<td colspan="2"><input type="submit"
									class="btn btn-success py-3 px-5" value="전송"> <input
									type="reset" class="btn btn-warning py-3 px-5" value="취소"></td>
							</tr>


						</table>
					</form>
				</div>

			</div>
		</div>
	</section>
	<%@ include file="/views/footer.jsp"%>
	<!-- loader -->
	<div id="ftco-loader" class="show fullscreen">
		<svg class="circular" width="48px" height="48px">
      <circle class="path-bg" cx="24" cy="24" r="22" fill="none"
				stroke-width="4" stroke="#eeeeee" />
      <circle class="path" cx="24" cy="24" r="22" fill="none"
				stroke-width="4" stroke-miterlimit="10" stroke="#F96D00" /></svg>
	</div>

	<%@ include file="/views/jsLoader.jsp"%>
</body>
</html>
