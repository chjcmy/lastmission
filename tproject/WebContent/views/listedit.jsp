<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<%@ include file="/views/cssLoader.jsp"%>
</head>
<body>
	<%@ include file="/views/header.jsp"%>
	<section class="ftco-section">

		<div class="container">
			<div class="row">

				<div style="z-index: 1020" class="col-md-12">
					<div class="list-group panel panel-success hidden">
						<div class="panel-heading list-group-item text-center hidden-xs">
							<h4>예약 편집</h4>
						</div>


					</div>
				</div>

				<div class="col-md-12">

					<div class="table-responsive">
						<table class="table table-striped table-bordered table-hover">
							<tr>
								<th>독서실</th>
								<td><label>독서실 이름</label></td>
							</tr>
							
							<tr>
								<th>날짜 변경</th>
								<td><input type="text" id="checkin_date" class="form-control" placeholder="날짜 선택"></td>
								<td><input type="text" id="checkin_time" class="form-control" placeholder="시작시간"></td>
							</tr>

							<tr>
								<th>할 말</th>
								<td><input type="password" class="form-control"
									name="pass2"></td>
							</tr>

							<tr>

								<th><label for="#">문서 추가</label></th>
								<td><input type="file" name="uploadFile"
									multiple="multiple"></td>


							</tr>


							</tbody>
						</table>
					</div>

				</div>
			</div>
		</div>
	</section>

	<br>



	<%@ include file="footer.jsp"%>
	<!-- loader -->
	<div id="ftco-loader" class="show fullscreen">
		<svg class="circular" width="48px" height="48px">
      <circle class="path-bg" cx="24" cy="24" r="22" fill="none"
				stroke-width="4" stroke="#eeeeee" />
      <circle class="path" cx="24" cy="24" r="22" fill="none"
				stroke-width="4" stroke-miterlimit="10" stroke="#F96D00" /></svg>
	</div>

	<%@ include file="jsLoader.jsp"%>
</body>
</html>