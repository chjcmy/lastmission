<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<nav
	class="navbar navbar-expand-lg navbar-dark ftco_navbar bg-dark ftco-navbar-light"
	id="ftco-navbar">
	<div class="container">
		<a class="navbar-brand"
			href="${pageContext.request.contextPath}/index/index.do">dokseosil</a>
		<button class="navbar-toggler" type="button" data-toggle="collapse"
			data-target="#ftco-nav" aria-controls="ftco-nav"
			aria-expanded="false" aria-label="Toggle navigation">
			<span class="oi oi-menu"></span> Menu
		</button>

		<div class="collapse navbar-collapse" id="ftco-nav">
			<ul class="navbar-nav ml-auto">
				<li class="nav-item active"><a
					href="${pageContext.request.contextPath}/index/index.do"
					class="nav-link">Home</a></li>

				<c:choose>
					<c:when test="${null eq sessionScope.userID}">
						<button type="button" id="loginButton" class="btn btn-info btn-lg"
							data-toggle="modal" data-target="#loginModal">로그인</button>

					</c:when>
					<c:otherwise>
						<c:choose>
							<c:when test="${'1' eq sessionScope.userGrade}">
								<li class="nav-item"><a
									href="${pageContext.request.contextPath}/user/mylist.do"
									class="nav-link">나의 예약 내역</a></li>
								<li class="nav-item"><a
									href="${pageContext.request.contextPath}/user/update.do"
									class="nav-link">마이페이지</a></li>
								<li class="nav-item"><a
									href="${pageContext.request.contextPath}/user/logout.do"
									class="nav-link">로그아웃</a></li>
							</c:when>
							<c:when test="${'2' eq sessionScope.userGrade}">
								<li class="nav-item"><a
									href="${pageContext.request.contextPath}/cafe/regist.do"
									class="nav-link">카페등록</a></li>
								<li class="nav-item"><a
									href="${pageContext.request.contextPath}/user/cafelist.do"
									class="nav-link">우리 매장 예약 내역</a></li>
								<li class="nav-item"><a
									href="${pageContext.request.contextPath}/user/update.do"
									class="nav-link">마이페이지</a></li>
								<li class="nav-item"><a
									href="${pageContext.request.contextPath}/user/logout.do"
									class="nav-link">로그아웃</a></li>
							</c:when>
						</c:choose>
					</c:otherwise>
				</c:choose>
			</ul>
		</div>
	</div>
</nav>

<!-- End nav-->
<!--  모달부분 -->
<div class="modal fade" id="loginModal" tabindex="1" role="dialog"
	aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<form action="${pageContext.request.contextPath}/user/login.do"
			method="post" class="search-destination" onsubmit="return check();">
			<div class="modal-content">
				<div class="modal-header">
					<h4 class="modal-title" id="loginModalLabel">회원 로그인</h4>
				</div>
				<div class="modal-body">
					<div class="form-field">
						<div class="icon">
							<span class="icon-id-badge"></span>&nbsp; 아이디
						</div>
						<input type="text" name="loginID" class="form-control"
							placeholder="아아디를 입력해주세요">
					</div>
					<br>
					<div class="form-field">
						<div class="icon">
							<span class="icon-compass"></span>&nbsp; 패스워드
						</div>
						<input type="password" name="loginPW" class="form-control"
							placeholder="패스워드를 입력해주세요">
					</div>
					<br>
					<div class="g-recaptcha"
						data-sitekey="6LcvOaUUAAAAAPtKq4J9QyuI3F_ZodKRNF6p6btV"></div>
					<br> <a
						href="${pageContext.request.contextPath}/user/regist.do" class=""
						style="color: blue">회원가입 </a> <a
						href="${pageContext.request.contextPath}/user/findid.do" class=""
						style="">아이디 및 비밀번호를 잊으셨나요?</a>

				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">로그인
						창 닫기</button>
					<button type="submit" class="btn btn-primary">로그인</button>
				</div>
			</div>
		</form>
	</div>
</div>
<!-- End 모달 -->


<section class="home-slider owl-carousel">
	<div class="slider-item"
		style="background-image: url('${pageContext.request.contextPath}/images/i1.jpg');">
		<div class="overlay"></div>
		<div class="container">
			<div class="row slider-text align-items-center">
				<div class="col-md-7 col-sm-12 ftco-animate">
					<h1 class="mb-3">시작이 반이다</h1>
					<h1 class="mb-3">스터디 카페</h1>
				</div>
			</div>
		</div>
	</div>

	<div class="slider-item"
		style="background-image: url('${pageContext.request.contextPath}/images/i2.png');">
		<div class="overlay"></div>
		<div class="container">
			<div class="row slider-text align-items-center">
				<div class="col-md-7 col-sm-12 ftco-animate">
					<h1 class="mb-3">어서오세요</h1>
					<h1 class="mb-3">스터디 카페</h1>
				</div>
			</div>
		</div>
	</div>

	<div class="slider-item"
		style="background-image: url('${pageContext.request.contextPath}/images/i3.jpg');">
		<div class="overlay"></div>
		<div class="container">
			<div class="row slider-text align-items-center">
				<div class="col-md-7 col-sm-12 ftco-animate">
					<h1 class="mb-3">스터디 카페</h1>
				</div>
			</div>
		</div>
	</div>
</section>
<!-- END slider -->
