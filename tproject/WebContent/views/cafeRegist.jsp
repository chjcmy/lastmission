<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<title>Voyage - Free Bootstrap 4 Template by Colorlib</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<%@ include file="/views/cssLoader.jsp"%>

</head>

<body>
	<%@ include file="/views/header.jsp"%>

	<!-- END nav -->
	<section class="ftco-section-2">
		<div class="container w-75 mx-auto mt-5">
			<h2>카페등록</h2>
			<form action="${pageContext.request.contextPath}/cafe/regist.do" class="bs-example form-horizontal" method="POST" enctype="multipart/form-data">
				<div class="row">
					<div class="col-sm12 col-md-3">
						<label for="cafeName">카페명</label>
					</div>
					<div class="col-sm12 col-md-9">
						<div class="form-group">
							<div class="form-field">
								<input type="text" name="cafeName" class="form-control">
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-sm12 col-md-3">
						<label for="cafeLocation">위치</label>
					</div>
					<div class="col-sm12 col-md-9">
						<div class="form-group">
							<div class="form-field">
								<input type="text" name="cafeLocation" class="form-control">
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-sm12 col-md-3">
						<label for="cafePrice">가격</label>
					</div>
					<div class="col-sm12 col-md-9">
						<div class="form-group">
							<div class="form-field">
								<input type="text" name="cafePrice" class="form-control">
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-sm12 col-md-3">
						<label for="cafePrice">위도 </label>
					</div>
					<div class="col-sm12 col-md-9">
						<div class="form-group">
							<div class="form-field">
								<input type="text" name="clat" class="form-control">
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-sm12 col-md-3">
						<label for="cafePrice">경도</label>
					</div>
					<div class="col-sm12 col-md-9">
						<div class="form-group">
							<div class="form-field">
								<input type="text" name="clong" class="form-control">
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-sm12 col-md-3">
						<label>예약시간</label>
					</div>

					<div class="col-sm-12 col-md-3">
						<div class="fields d-block d-lg-flex">
							<select class="form-control" name="cafeStart" id="start-time">
								<option value="">시작시간</option>

							</select> <select class="form-control" name="cafeEnd" id="end-time">
								<option value="">종료시간</option>
							</select>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-12 col-md-3">
						<label for="cafeBody">내용</label>
					</div>

					<div class="col-sm-12 col-md-9">
						<div class="form-group">
							<div class="form-field">
								<textarea class="form-control" name="cafeBody" rows="3" id="textArea"></textarea>
							</div>
						</div>
					</div>
				</div>

				<div class="row">
					<div class="col-sm-12 col-md-3">
						<label for="#">배치도</label>
					</div>

					<div class="col-sm-12 col-md-9">
						<div class="form-group">
							<div class="form-field">
								<input type="file" name="uploadFile1">
							</div>
						</div>
					</div>
				</div>
				<hr>
				<div class="row">
					<div class="col-sm-12 col-md-3">
						<label for="#">실내사진(최대 2장)</label>
					</div>

					<div class="col-sm-12 col-md-9">
						<div class="form-group">
							<div class="form-field">
								<input type="file" name="uploadFile2" multiple="multiple">
							</div>
						</div>
					</div>
				</div>
				<hr>
				<div class="row">
					<div class="col-sm-12 col-md-3">
						<label for="#">테이블 배치</label>
					</div>
					<div class="col-sm-12 col-md-9">
						<input type='number' id='tcnt' value='0' /> <input id='btn-add-row' type="button" value="추가"> <input id='btn-delete-row' type="button" value="제거">
						<hr>
						<table id="mytable">
							<thead>
								<th>테이블</th>
								<th>인원제한</th>
							</thead>
							<tbody></tbody>
						</table>
					</div>
				</div>
				<hr>
				<div class="row">
					<div class="col-md-12 align-self-end">
						<div class="form-group" align="center">
							<div class="form-field">
								<button type="submit" class="btn btn-success">확인</button>
								<button type="reset" class="btn btn-danger">취소하기</button>
							</div>
						</div>
					</div>
				</div>
			</form>
		</div>
	</section>

	<footer class="ftco-footer ftco-bg-dark ftco-section"> </footer>

	<!-- loader -->
	<div id="ftco-loader" class="show fullscreen">
		<svg class="circular" width="48px" height="48px">
      <circle class="path-bg" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke="#eeeeee" />
      <circle class="path" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke-miterlimit="10" stroke="#F96D00" /></svg>
	</div>
	<%@include file="jsLoader.jsp"%>


	<script>
		var st = 1;
		for(var i=1;i<=24;i++){
			$('#start-time').append("<option value='"+i+"'>"+i+"</option>");
		}
		// 종료시간 값넣기
		$('#start-time').change(function(){
			$('#end-time').empty();
			var st=$('#start-time option:checked').text();
			st=Number(st);
			st+=1;
			for(st;st<=24;st++){
				$('#end-time').append("<option value='"+st+"'>"+st+"</option>");		
			}
		});
		
		//행추가하기
		$('#btn-add-row')
				.click(
						function() {
							var cnt = $('#tcnt').val();
							if(cnt < 0){
								alert("0이상 입력하세요");
							}
							else{
								var time = new Date().toLocaleTimeString();

								for (var i = 1; i <= cnt; i++) {
									$('#mytable > tbody:last')
											.append(
													'<tr><td>'
															+ i
															+ " 번 테이블</td><td> <input type='number' name='tableData' value='0' placeholder='인원'>"
															+ '</td></tr>');
								}	
							}
						});
		$('#btn-delete-row').click(function() {
			$('#mytable > tbody:last > tr:last').remove();
		});
	</script>

	<!-- 이미지 업로드 관련 -->

	<!-- end 이미지관련 -->
</body>
</html>