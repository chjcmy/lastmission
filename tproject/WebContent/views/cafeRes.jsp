<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<title>Voyage - Free Bootstrap 4 Template by Colorlib</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<%@ include file="cssLoader.jsp"%>

<style>
#fileDragDesc {
	width: 100%;
	height: 100%;
	margin-left: auto;
	margin-right: auto;
	padding: 5px;
	text-align: center;
	line-height: 300px;
	vertical-align: middle;
}
</style>
</head>

<body>
	<%@ include file="header.jsp"%>
	<!-- END nav -->

	<section class="ftco-section-2">
		<div class="container w-75 mx-auto mt-5">
			<h2>예약</h2>

			<form name="uploadForm" id="uploadForm" class="bs-example form-horizontal" method="POST">
				<input type="hidden" id="cafeId" name="cafeId" class="form-control" readonly value="${cafeInfo.cafeId}">

				<div class="row">
					<div class="col-sm12 col-md-3">
						<label for="#">카페명</label>
					</div>
					<div class="col-sm12 col-md-9">
						<div class="form-group">
							<div class="form-field">
								<input type="text" name="cafeName" class="form-control" readonly value="${cafeInfo.cafeName}">
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-sm12 col-md-3">
						<label for="#">위치</label>
					</div>
					<div class="col-sm12 col-md-9">
						<div class="form-group">
							<div class="form-field">
								<input type="text" name="cafeLocation" class="form-control" readonly value="${cafeInfo.cafeLocation}">
							</div>
						</div>
					</div>
				</div>
				<br>
				<div class="row">
					<div class="col-sm12 col-md-3">
						<label for="#">날짜 선택</label>
					</div>
					<div class="col-sm-12 col-md-3">
						<input type="text" name="resDate" id="datepicker" class="form-control" placeholder="날짜 선택">
					</div>
				</div>
				<br>
				<div class="row">
					<div class="col-sm12 col-md-3">
						<label for="#">테이블 선택</label>
					</div>

					<div class="col-sm-12 col-md-3">
						<div class="block-17">
							<select id="tableNum" name="tableNum" class="form-control">
								<option value="">테이블 선택</option>

							</select>
						</div>
					</div>
					<div class="col-sm-12 col-md-3">
						<input type="button" id="searchRes" value="검색" class="form-control btn btn-success">
					</div>
				</div>

				<br>
				<div class="row">
					<div class="col-sm12 col-md-3">
						<label for="#">예약시간</label>
					</div>

					<div class="col-sm-12 col-md-3">
						<div class="fields d-block d-lg-flex">
							<select class="form-control" name="startT" id="start-time">
								<option value="">시작시간</option>

							</select> <select class="form-control" name="endT" id="end-time">
								<option value="">종료시간</option>
							</select>
						</div>
					</div>
				</div>
				<br>
				<div class="row">
					<div class="col-sm12 col-md-3">
						<label for="#">가격</label>
					</div>
					<div class="col-sm12 col-md-9">
						<div class="form-group">
							<div class="form-field">
								<input type="text" id="resCost" name="resCost" class="form-control" readonly value="0">
							</div>
						</div>
					</div>
				</div>
				<br>
				<div class="row">
					<div class="col-sm-12 col-md-3">
						<label for="#">요청사항</label>
					</div>

					<div class="col-sm-12 col-md-9">
						<div class="form-group">
							<div class="form-field">
								<textarea class="form-control" name="resText" rows="3" id="textArea"></textarea>
							</div>
						</div>
					</div>
				</div>

				<!-- ajax 파일 드래그 -->
				<div class="row">
					<div class="col-sm-12 col-md-3">
						<label for="#">문서 추가</label>
					</div>
					<div class="col-sm-12 col-md-9">
						<div class="form-group">
							<div class="form-field">
								<div id="dropZone" style="width: 100%; height: 300px; border-style: solid; border-color: black;">
									<div id="fileDragDesc">파일을 드래그 해주세요.</div>
									<table id="fileListTable" width="100%" border="0px">
										<tbody id="fileTableTbody">

										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>
				<!-- ajax 파일 드래그 -->

				<div class="row">
					<div class="col-md-12 align-self-end">
						<div class="form-group" align="center">
							<div class="form-field">
								<button type="submit" id="btnsubmit" class="btn btn-success">예약</button>
								<button type="reset" class="btn btn-danger">취소</button>
							</div>
						</div>
					</div>
				</div>
				<select style="display: none" id="resCount" name="resCount" class="form-control">

				</select>
			</form>

		</div>
	</section>
	<footer class="ftco-footer ftco-bg-dark ftco-section"> </footer>



	<!-- loader -->
	<div id="ftco-loader" class="show fullscreen">
		<svg class="circular" width="48px" height="48px">
      <circle class="path-bg" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke="#eeeeee" />
      <circle class="path" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke-miterlimit="10" stroke="#F96D00" /></svg>
	</div>
	<%@include file="jsLoader.jsp"%>
	<script src="${pageContext.request.contextPath}/js/uploadjs.js"></script>
	<script>
	//테이블에 예약시간 가져오기
		var arr = new Array();
		<c:forEach var="time" items="${timeTable}" varStatus="status">
			arr.push("${time}");
			<c:if test="${time == true }">
				$('#start-time').append("<option value='${status.index}'>${status.index}</option>");
			</c:if>
		</c:forEach>
	
		$('#start-time').change(function(){
			$('#end-time').empty();
			var st=$('#start-time option:checked').text();
			st=Number(st);
			while(true){
				if(arr[st] == true){
					$('#end-time').append("<option value='"+(st+1)+"'>"+(st+1)+"</option>");		
				}
				else
					break;
				st++;
			}
		});
		$('#end-time').change(function(){
			var costPH=${cafeInfo.cafePrice};
			var st=$('#start-time option:checked').text();
			var et=$('#end-time option:checked').text();
			var idx=$('#tableNum option').index($("#tableNum option:selected"));
			var tt=$("#resCount option:eq("+(idx-1)+")").val();
			$('#resCost').val((et-st)*costPH*tt);
		});
		//테이블 번호와 최대인원 보여주기
		<c:forEach var="table" items="${tables}">
			$('#tableNum').append("<option value='${table.tableNum}'>${table.tableNum}번 최대인원(${table.tableSize})</option>");
			$('#resCount').append("<option value='${table.tableSize}'></option>");
		</c:forEach>
		
		//검색 했을때 작동 
		$('#searchRes').click(function(){
			var cafeStart=${cafeInfo.cafeStart};
			var cafeEnd=${cafeInfo.cafeEnd};
			var cafeId=$('#cafeId').val();
			var date =$('#datepicker').val();
			var tableNum=$('#tableNum option:checked').val();
			//alert(date+' '+tableNum);
			$.ajax({
				url :"${pageContext.request.contextPath}/res/resFind.do",
				type : "POST",
				data :JSON.stringify({"date":date ,"tableNum":tableNum,"cafeStart":cafeStart,"cafeEnd":cafeEnd, "cafeId":cafeId}),
				dataType : "json",
				contentType : "application/json; charset=UTF-8",
				success : function(data){
						console.log(data);
						arr=data;
						$('#start-time').empty();
						for(var i=0;i<arr.length;i++){
							if(arr[i]==true)
								$('#start-time').append("<option value='"+i+"'>"+i+"</option>");
						}
				},
				error:function(errMsg){
					alert(errMsg);
				}
			})
		});
	</script>
	<script>
		$(function() {
	        $('#datepicker').datepicker({
	            dateFormat: 'yyyy-mm-dd'
	        	,autoclose: true
	        });                    
	        $('#datepicker').datepicker('setDate', 'today');       
	    });
	</script>

</body>
</html>