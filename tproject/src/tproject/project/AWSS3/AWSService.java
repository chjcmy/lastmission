package tproject.project.AWSS3;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.UUID;

import com.amazonaws.AmazonServiceException;
import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.GeneratePresignedUrlRequest;
import com.amazonaws.services.s3.model.ObjectMetadata;

public class AWSService {
	private static final String Bucket_Name = "lastweb-pro";
	private static final String Access_Key = "AKIAS5ZK22BBFYIMU5GU";
	private static final String Secret_key = "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";

	private AmazonS3 amazonS3;

	public AWSService() {
		AWSCredentials asAwsCredentials = new BasicAWSCredentials(Access_Key, Secret_key);
		amazonS3 = new AmazonS3Client(asAwsCredentials);
		amazonS3.setEndpoint("s3.ap-northeast-2.amazonaws.com");
	}

	// 파일 업로드
	public String fileUpload(String uploadPath, String fileName, byte[] fileData) throws FileNotFoundException {
		String uploadedFileName = null;
		if (amazonS3 != null) {
			try {
				UUID uid = UUID.randomUUID();
				String savedName = "/" + uid.toString() + "_" + fileName;
				uploadedFileName = (uploadPath + savedName).replace(File.separatorChar, '/');
				ObjectMetadata metaData = new ObjectMetadata();
				metaData.setContentLength(fileData.length);
				ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(fileData);
				amazonS3.putObject(Bucket_Name, uploadedFileName, byteArrayInputStream, metaData);
			} catch (AmazonServiceException ase) {
				ase.printStackTrace();
			}
		}
		return uploadedFileName;
	}

	// 파일 삭제
	public void fileDelete(String uploadPath, String fileName) {

		String imgName = (uploadPath + fileName).replace(File.separatorChar, '/');
		amazonS3.deleteObject(Bucket_Name, imgName);
		System.out.println("삭제성공");
	}

	// 파일 URL
	public String getFileURL(String fileName) {
		System.out.println("넘어오는 파일명 : " + fileName);
		String imgName = (fileName).replace(File.separatorChar, '/');
		System.out.println(amazonS3.getUrl(Bucket_Name, imgName));
		return amazonS3.generatePresignedUrl(new GeneratePresignedUrlRequest(Bucket_Name, imgName)).toString();
	}
	
	
	
}
