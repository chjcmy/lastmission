package tproject.project.domain;



import java.io.Serializable;
import java.util.List;

// 占쎌삂占쎄쉐占쎌쁽 : 獄쏅벡�뜚筌욑옙 - 占쎌뵠占쎄맒占쎌뒭
public class CafeInfo implements Serializable {

	private static final long serialVersionUID = -7755173628930202505L;

		private int cafeId; // 燁삳똾�읂 ID
		private String cafeName; // 燁삳똾�읂筌륅옙 
		private String cafeLocation; // 燁삳똾�읂 筌욑옙占쎈열 
		private String userId;
		//location table�⑨옙 join 占쎈퉸占쎄퐣 占쎌젟癰귣�占쏙옙 揶쏉옙占쎌죬占쎌궞 占쎌굙占쎌젟
		private String cafeStart; // 占쎌궎占쎈탞 占쎈뻻揶쏉옙
		private String cafeEnd; // 筌띾뜃而� 占쎈뻻揶쏉옙
		private String cafeBody; // 燁삳똾�읂 占쎄맒占쎄쉭占쎄땀占쎌뒠
		private int cafePrice; // 1占쎌뵥占쎈뼣 筌욑옙�겫占� 揶쏉옙野껓옙 
		private String cafePicture1; // 燁삳똾�읂 占쎄텢筌욑옙 1
		private String cafePicture2; // 燁삳똾�읂 占쎄텢筌욑옙 2
		private String cafePicture3; // 燁삳똾�읂 占쎈�믭옙�뵠�뇡占� 占쎄텢筌욑옙 	
		private String clat;
		private String clong;
		private List<CafeTable> cafeTables;
		
		public int getCafeId() {
			return cafeId;
		}
		public void setCafeId(int cafeId) {
			this.cafeId = cafeId;
		}
		public String getCafeName() {
			return cafeName;
		}
		public void setCafeName(String cafeName) {
			this.cafeName = cafeName;
		}
		public String getCafeLocation() {
			return cafeLocation;
		}
		public void setCafeLocation(String cafeLocation) {
			this.cafeLocation = cafeLocation;
		}
		public String getCafeStart() {
			return cafeStart;
		}
		public void setCafeStart(String cafeStart) {
			this.cafeStart = cafeStart;
		}
		public String getCafeEnd() {
			return cafeEnd;
		}
		public void setCafeEnd(String cafeEnd) {
			this.cafeEnd = cafeEnd;
		}
		public String getCafeBody() {
			return cafeBody;
		}
		public void setCafeBody(String cafeBody) {
			this.cafeBody = cafeBody;
		}
		public int getCafePrice() {
			return cafePrice;
		}
		public void setCafePrice(int cafePrice) {
			this.cafePrice = cafePrice;
		}
		public String getCafePicture1() {
			return cafePicture1;
		}
		public void setCafePicture1(String cafePicture1) {
			this.cafePicture1 = cafePicture1;
		}
		public String getCafePicture2() {
			return cafePicture2;
		}
		public void setCafePicture2(String cafePicture2) {
			this.cafePicture2 = cafePicture2;
		}
		public String getCafePicture3() {
			return cafePicture3;
		}
		public void setCafePicture3(String cafePicture3) {
			this.cafePicture3 = cafePicture3;
		}
		public List<CafeTable> getCafeTables() {
			return cafeTables;
		}
		public void setCafeTables(List<CafeTable> cafeTables) {
			this.cafeTables = cafeTables;
		}
		public String getUserId() {
			return userId;
		}
		public void setUserId(String userId) {
			this.userId = userId;
		}
		public String getClat() {
			return clat;
		}
		public void setClat(String clat) {
			this.clat = clat;
		}
		public String getClong() {
			return clong;
		}
		public void setClong(String clong) {
			this.clong = clong;
		}
		
}
