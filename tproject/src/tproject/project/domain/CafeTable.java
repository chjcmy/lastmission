package tproject.project.domain;

import java.io.Serializable;

public class CafeTable  implements Serializable {

	private static final long serialVersionUID = -7755173628930202505L;
	private String tableId;
	private String studyId;
	private String tableNum;
	private String tableSize;
	public String getTableId() {
		return tableId;
	}
	public void setTableId(String tableId) {
		this.tableId = tableId;
	}
	public String getStudyId() {
		return studyId;
	}
	public void setStudyId(String studyId) {
		this.studyId = studyId;
	}
	public String getTableNum() {
		return tableNum;
	}
	public void setTableNum(String tableNum) {
		this.tableNum = tableNum;
	}
	public String getTableSize() {
		return tableSize;
	}
	public void setTableSize(String tableSize) {
		this.tableSize = tableSize;
	}
}
