package tproject.project.domain;

import java.io.Serializable;
import java.sql.Date;

public class ResInfo implements Serializable {

	private static final long serialVersionUID = -7755173628930202505L;
	// 아이디 카페이름 날짜 시작시간 끝시간 파일첨부 
	private String userId;
	private int cafeId;
	private Date resDate;
	private int startT;
	private int endT;
	private String resCount;
	private String fileL;
	private String tableNum;
	private String resCost;
	private String resText;
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	
	public int getCafeId() {
		return cafeId;
	}
	public void setCafeId(int cafeId) {
		this.cafeId = cafeId;
	}
	public Date getResDate() {
		return resDate;
	}
	public void setResDate(Date resDate) {
		this.resDate = resDate;
	}
	public int getStartT() {
		return startT;
	}
	public void setStartT(int startT) {
		this.startT = startT;
	}
	public int getEndT() {
		return endT;
	}
	public void setEndT(int endT) {
		this.endT = endT;
	}
	public String getFileL() {
		return fileL;
	}
	public void setFileL(String fileL) {
		this.fileL = fileL;
	}
	public String getTableNum() {
		return tableNum;
	}
	public void setTableNum(String tableNum) {
		this.tableNum = tableNum;
	}
	public String getResCost() {
		return resCost;
	}
	public void setResCost(String resCost) {
		this.resCost = resCost;
	}
	public String getResText() {
		return resText;
	}
	public void setResText(String resText) {
		this.resText = resText;
	}

	public String getResCount() {
		return resCount;
	}
	public void setResCount(String resCount) {
		this.resCount = resCount;
	}
}
