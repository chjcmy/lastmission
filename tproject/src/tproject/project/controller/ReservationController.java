package tproject.project.controller;

import java.io.BufferedOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.sql.Date;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.fasterxml.jackson.databind.ObjectMapper;

import tproject.project.AWSS3.AWSService;
import tproject.project.domain.CafeInfo;
import tproject.project.domain.CafeTable;
import tproject.project.domain.ResInfo;
import tproject.project.domain.UserTable;
import tproject.project.service.CafeInfoService;
import tproject.project.service.CafeTableService;
import tproject.project.service.ResService;

@Controller
@RequestMapping("res")
public class ReservationController {
	@Autowired
	CafeInfoService cafeInfoService;
	@Autowired
	ResService resService;
	@Autowired
	CafeTableService tableService;

	AWSService s3 = new AWSService();
	
	@RequestMapping(value = "res.do", method = RequestMethod.GET)
	public String reserve(HttpSession session, Model model, int cafeId) {

		CafeInfo cafeInfo = cafeInfoService.findContent_detail(cafeId);
		List<CafeTable> tables = tableService.findAllTables(cafeId);

		model.addAttribute("tables", tables);
		model.addAttribute("cafeInfo", cafeInfo);
		session.setAttribute("cafeId", cafeId);
		return "cafeRes";
	}

	@RequestMapping(value = "res.do", method = RequestMethod.POST)
	public void reserve(HttpSession session, MultipartHttpServletRequest multi) {
		ResInfo resInfo = new ResInfo();
		resInfo.setUserId(session.getAttribute("userID").toString());
		
		// 아이디 세션에서 가져오면됨
		resInfo.setCafeId(Integer.valueOf(multi.getParameter("cafeId")));
		System.out.println("id" + Integer.valueOf(multi.getParameter("cafeId")));
		resInfo.setResCost(multi.getParameter("resCost"));
		resInfo.setResCount(multi.getParameter("resCount"));
		resInfo.setEndT(Integer.valueOf(multi.getParameter("endT")));
		resInfo.setStartT(Integer.valueOf(multi.getParameter("startT")));
		resInfo.setTableNum(multi.getParameter("tableNum"));
		resInfo.setResDate(Date.valueOf(multi.getParameter("resDate")));
		resInfo.setResText(multi.getParameter("resText"));
		Iterator<String> files = multi.getFileNames();
		if (files.hasNext()) {
			String uploadFile = files.next();
			MultipartFile mFile = multi.getFile(uploadFile);
			resService.registRes(resInfo, mFile);
			System.out.println("er3");
		} else {
			resService.registRes(resInfo);
		}
		//return "redirect:/cafe/cafeList.do";
	}

	@RequestMapping(value = "chg.do")
	public String chg() {
		System.out.println("paths");
		return "redirect:/index/index.do";
	}
	@RequestMapping(value ="down.do")
	public String down(HttpServletResponse response,UserTable userTable) {		
		
		String urlPath =s3.getFileURL(userTable.getFile());
        //String fileName = (String) param.get("fileName"); //파일명
        
        try {
            URL url = new URL(urlPath);           
            response.setHeader("Content-disposition", "attachment;filename=" + userTable.getFile());
            response.setContentType("application/octer-stream");
            InputStream is = url.openStream();
 
            BufferedOutputStream outs = new BufferedOutputStream(response.getOutputStream());
            int len;
            byte[] buf = new byte[1024];
            while ( (len = is.read(buf)) > 0 ) {
                outs.write(buf, 0, len);
            }
            outs.close();
 
        } catch (Exception e) {                    
            e.printStackTrace();
        }            
		return "redirect:/user/mylist.do";
	}
	@RequestMapping(value ="down2.do")
	public String down2(HttpServletResponse response,HttpSession session,UserTable userTable) {		
		
		String urlPath =s3.getFileURL(userTable.getFile());
        //String fileName = (String) param.get("fileName"); //파일명
        
        try {
            URL url = new URL(urlPath);           
            response.setHeader("Content-disposition", "attachment;filename=" + userTable.getFile());
            response.setContentType("application/octer-stream");
            InputStream is = url.openStream();
 
            BufferedOutputStream outs = new BufferedOutputStream(response.getOutputStream());
            int len;
            byte[] buf = new byte[1024];
            while ( (len = is.read(buf)) > 0 ) {
                outs.write(buf, 0, len);
            }
            outs.close();
 
        } catch (Exception e) {                    
            e.printStackTrace();
        }            
		return "redirect:/user/cafelist.do";
	}
	@ResponseBody
	@RequestMapping(value = "resFind.do", method = RequestMethod.POST)
	public String resFind(@RequestBody Map<String, String> params, HttpServletResponse response) {
		List<ResInfo> resList = new ArrayList<ResInfo>();
		Date rd = java.sql.Date.valueOf(params.get("date"));
		System.out.println(params.get("cafeId"));
		resList = resService.showRes(rd, Integer.valueOf(params.get("cafeId")), params.get("tableNum"));

		boolean timeTable[] = new boolean[24];
		int cStart = Integer.valueOf(params.get("cafeStart"));
		int cEnd = Integer.valueOf(params.get("cafeEnd"));
		Arrays.fill(timeTable, true);
		for (int i = 0; i < cStart; i++) {
			timeTable[i] = false;
		}
		for (int i = cEnd; i < 24; i++) {
			timeTable[i] = false;
		}
		for (ResInfo re : resList) {
			System.out.println(re.getStartT());
			System.out.println(re.getEndT());
			for (int i = re.getStartT(); i < re.getEndT(); i++) {
				timeTable[i] = false;
			}
		}
		String str = "";
		ObjectMapper mapper = new ObjectMapper();
		try {
			str = mapper.writeValueAsString(timeTable);
		} catch (Exception e) {
			System.out.println("first() mapper   ::    " + e.getMessage());
		}
		return str;
	}
	
	

}
