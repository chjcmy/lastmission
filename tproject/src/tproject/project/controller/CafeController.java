package tproject.project.controller;

import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import tproject.project.domain.CafeInfo;
import tproject.project.domain.CafeTable;
import tproject.project.domain.PageMaker;
import tproject.project.service.CafeInfoService;
import tproject.project.service.CafeTableService;

@Controller
@RequestMapping("cafe")
public class CafeController {

	@Autowired
	private CafeInfoService cafeservice;
	@Autowired
	private CafeTableService tableService;
	@RequestMapping("info.do")
	public String findAll() {
		return "index";
	}

	@RequestMapping(value="regist.do" ,method=RequestMethod.GET)
	public String regist() {
		return "cafeRegist";
	}
	@RequestMapping(value = "regist.do", method = RequestMethod.POST)
	public String regist(HttpSession session, CafeInfo cafeInfo,
			@RequestParam(value = "tableData", required = false) List<String> tables,
			@RequestParam(value = "uploadFile1", required = false) MultipartFile floorFile,
			@RequestParam(value = "uploadFile2", required = false) List<MultipartFile> imgFiles) {
		List<CafeTable> cafeTables=tableService.groupTable(tables,
		cafeInfo.getCafeId());
		cafeInfo.setCafeTables(cafeTables);
		cafeInfo.setUserId(session.getAttribute("userID").toString());
		cafeservice.registerStudy(cafeInfo,floorFile,imgFiles);
		return "redirect:/index/index.do";
	}
	@RequestMapping(value = "cafeList.do", method = RequestMethod.GET)
	// value <- jsp form 태그에 들어가기 위해 경로 이름 설정
	public String pagelist(HttpSession session,Model model, @RequestParam(value = "location", required=false) String location, @RequestParam(value = "pagenum", required=false) String pagenum, @RequestParam(value = "contentnum", required=false) String contentnum) {
		if (pagenum == null) {
			pagenum = "1";
			}
		if (contentnum == null) {
			contentnum = "10";
			}
		
		PageMaker pagemaker = new PageMaker();
		
		int pagenum1 = Integer.parseInt(pagenum);
        int contentnum1 = Integer.parseInt(contentnum);


		pagemaker.setTotalcount(cafeservice.totalcount(location));
		System.out.println(cafeservice.totalcount(location));
		pagemaker.setPagenum(pagenum1 -1);
		pagemaker.setCurrentblock(pagenum1);
		pagemaker.setLastblock(pagemaker.getTotalcount());
		System.out.println(pagemaker.getEndPage());
		
		pagemaker.prevnext(pagenum1);
		pagemaker.setStartPage(pagemaker.getCurrentblock());
		pagemaker.setEndPage(pagemaker.getLastblock(), pagemaker.getCurrentblock());
		List<CafeInfo> cafeList = cafeservice.findContent(location, pagemaker.getPagenum() * 10, pagemaker.getContentnum());
		System.out.println("사이즈"+ cafeList.size());
		System.out.println(cafeList.size());
		model.addAttribute("cafeList", cafeList);
		model.addAttribute("page", pagemaker);
		return "cafeList";

	}

	// 이상우 - 스터디 상세내역
	@RequestMapping(value = "cafeDetail.do", method = RequestMethod.GET)
	public String Detail(Model model, int cafeId,HttpSession session) {
		CafeInfo cafeInfo = cafeservice.findContent_detail(cafeId);
		System.out.println(session.getAttribute("userID").toString());
		model.addAttribute("cafeInfo", cafeInfo);
		return "cafeDetail";

	}
}