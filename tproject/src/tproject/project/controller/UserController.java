package tproject.project.controller;

import java.security.MessageDigest;
import java.util.List;
import java.util.Random;

import javax.servlet.http.HttpSession;

import org.apache.commons.mail.HtmlEmail;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import tproject.project.domain.User;
import tproject.project.domain.UserTable;
import tproject.project.service.UserService;

@Controller
@RequestMapping("user")
public class UserController {
	@Autowired
	private UserService userservice;

	//회원가입 부분
	@RequestMapping(value = "regist.do", method = RequestMethod.POST)
	public String signup(HttpSession session, User user) {

		System.out.println(user.getUserPW());
		System.out.println();

		if (user.getUserPW().equals(user.getUserPW2())) {

			try {

				String Pw = encrypt(user.getUserPW());
				user.setUserPW(Pw);

				userservice.insertUser(user);
			} catch (Exception e) {

				session.setAttribute("success", false);
				System.out.println("회원가입 오류 메시지 출력");
				return "alert/signupAlert";
			}
			session.setAttribute("success", true);
		} else {
			session.setAttribute("success", false);
			System.out.println("컨트롤러측: 비밀번호 불일치!!");
			return "alert/signupAlert";
		}

		return "user/regist";
	}

	@RequestMapping(value = "regist.do", method = RequestMethod.GET)
	public String signup() {
		return "user/regist";
	}
	//로그인
	@RequestMapping(value = "login.do", method = RequestMethod.POST)
	public String login(@RequestParam(value = "loginID") String userID, @RequestParam(value = "loginPW") String userPW,
			HttpSession session) throws Exception {
		System.out.println(userID);
		userPW = encrypt(userPW);
		System.out.println(userPW);

		User user = userservice.loginUser(userID, userPW);
		if (user.getLogined() == true) {
			System.out.println("컨트롤러측: 로그인 성공!");

			session.setAttribute("userID", userID);
			session.setAttribute("userPW", userPW);
			session.setAttribute("userName", user.getUserName());
			session.setAttribute("userTel", user.getUserTel());
			session.setAttribute("userMail", user.getUserMail());
			session.setAttribute("userJob", user.getUserJob());
			session.setAttribute("userAge", user.getUserAge());
			session.setAttribute("userGrade", user.getUserGrade());

			session.setAttribute("success", true);

			System.out.println("회원정보 세션주입완료!");
			System.out.println("userID: " + userID + "\n" + "userPW: " + userPW + "\n" + "userName: "
					+ user.getUserName() + "\n" + "userJob: " + user.getUserJob() + "\n" + "userMail: "
					+ user.getUserMail() + "\n" + "userTel: " + user.getUserTel() + "\n" + "userGrade: "
					+ user.getUserGrade() + "userLogined: " + user.getLogined() + "\n");

			return "alert/loginAlert";

		} else {
			System.out.println("컨트롤러측: 로그인 실패!");
			session.setAttribute("success", false);

			return "alert/loginAlert";
		}

	}
	//로그아웃
	@RequestMapping(value = "logout.do")
	public String logout(HttpSession session) {

		session.invalidate();
		return "index";
	}
	//회원 수정
	@RequestMapping(value = "update.do")
	public String updateInfo(@RequestParam(value = "UserPW") String userPW,
			@RequestParam(value = "UserName") String userName, @RequestParam(value = "UserTel") String userTel,
			@RequestParam(value = "UserMail") String userMail, @RequestParam(value = "UserJob") String userJob,
			@RequestParam(value = "UserAge") String userAge, User user, Model model, HttpSession session)
			throws Exception {
		if (userPW.equals("")) {
			session.setAttribute("updateSuccess", false);
			return "alert/updateAlert";
		} else {

			String userID = (String) session.getAttribute("userID");
			System.out.println(userID);
			user.setUserID(userID);
			userservice.updateUser(user);
			session.setAttribute("loginID", userID);
			session.setAttribute("loginPW", user.getUserPW());
			session.setAttribute("userName", user.getUserName());
			session.setAttribute("userJob", user.getUserJob());
			session.setAttribute("userMail", user.getUserMail());
			session.setAttribute("userAge", user.getUserAge());
			session.setAttribute("userTel", user.getUserTel());
			session.setAttribute("userGrade", user.getUserGrade());

			System.out.println("회원정보 세션주입완료!\n새로운 계정정보");

			System.out.println("userID: " + userID + "\n" + "userPW: " + user.getUserPW() + "\n" + "userName: "
					+ user.getUserName() + "\n" + "userJob: " + user.getUserJob() + "\n" + "userMail: "
					+ user.getUserMail() + "\n" + "userTel: " + user.getUserTel() + "\n" + "userGrade: "
					+ user.getUserGrade() + "userAge: " + user.getUserAge() + "\n" + "userLogined: " + user.getLogined()
					+ "\n");

			session.setAttribute("updateSuccess", true);
			return "index";

		}

	}
	//회원 수정
	@RequestMapping(value = "update.do", method = RequestMethod.GET)
	public String updateInfo() {
		return "user/userInfoModify";
	}
	//회원 활성화,비활성화
	@RequestMapping(value = "deleteConfirm.do", method = RequestMethod.POST)
	public String delete(HttpSession session, Model model, @RequestParam(value = "inputpassword") String userPW) {

		String sessionPW = (String) session.getAttribute("userPW");
		System.out.println(session.getAttribute("userPW"));
		System.out.println(sessionPW);
		System.out.println(userPW);
		if (sessionPW.equals(userPW)) {
			try {
				userservice.deleteUser((String) session.getAttribute("loginID"), sessionPW);
				model.addAttribute("success", 1);
				session.invalidate();

				System.out.println("회원탈퇴 수행 완료!");
				return "alert/deleteAlert";
			} catch (Exception e) {
				model.addAttribute("success", 2);
				return "alert/deleteAlert";
			}
		} else {
			model.addAttribute("success", 3);
			return "alert/deleteAlert";
		}
	}
	//아이디찾기
	@RequestMapping(value = "findid.do", method = RequestMethod.GET)
	public String findid() {

		return "user/finduser";
	}
	//아이디찾기
	@RequestMapping(value = "findid.do", method = RequestMethod.POST, produces = "application/text; charset=utf8")
	@ResponseBody
	public String simpleWithObject(@RequestParam(value = "userName") String userName,
			@RequestParam(value = "userMail") String userMail) throws Exception {

		String id = null;

		System.out.println(userName);
		System.out.println(userMail);
		User user = userservice.findid(userName, userMail);
		System.out.println(user.getUserID());
		id = "아이디는 " + user.getUserID() + " 입니다.";
		return id;
	}
	//비밀번호 찾기
	@RequestMapping(value = "findpw.do", method = RequestMethod.POST, produces = "application/text; charset=utf8")
	@ResponseBody
	public String findpw(@RequestParam(value = "userID") String userID,
			@RequestParam(value = "userName") String userName, @RequestParam(value = "userMail") String userMail)
			throws Exception {

		String act = null;
		String pw = null;

		String newpw = createKey();
		System.out.println(newpw);

		send_mail(userID, newpw, userMail);

		
		  User user = userservice.findpw(userID, userName, userMail);
		  System.out.println(user.getAct()); act = user.getAct(); if ("0".equals(act))
		  { pw = "임시 암호가 이메일로 보내줬습니다.";
		  newpw = encrypt(newpw);
		  System.out.println(newpw);
		  userservice.changepw(userID, newpw);
		  } else if ("1".equals(act)) { 
			  pw = "탈퇴 회원입니다";
		  
		  } else { pw = "회원 정보가 없습니다"; }
		  
	
		return pw;
	}
	//나의 예약 리스트
	@RequestMapping(value = "mylist.do", method = RequestMethod.GET)
	public String userlist(Model model, HttpSession session) {
		String userID = (String) session.getAttribute("userID");
		List<UserTable> userList = userservice.userlist(userID);
		model.addAttribute("userList", userList);
		return "user/myList";
	}
	//카페 점주 예약 리스트
	@RequestMapping(value = "cafelist.do", method = RequestMethod.GET)
	public String cafelist(Model model, HttpSession session) {
		String userID = (String) session.getAttribute("userID");
		List<UserTable> userList = userservice.cafelist(userID);
		model.addAttribute("userList", userList);
		return "user/cafeList";
	}
	//이메일 보내기
	public void send_mail(String userID, String newpw, String userMail) {
		// Mail Server 설정
		String charSet = "utf-8";
		String hostSMTP = "smtp.gmail.com";
		String hostSMTPid = "chjcmy@gmail.com";
		String hostSMTPpwd = "aass5522";

		// 보내는 사람 EMail, 제목, 내용
		String fromEmail = userMail;

		String fromName = "Spring Homepage";
		String subject = "";
		String msg = "";

		subject = "Spring Homepage 임시 비밀번호 입니다.";
		msg += "<div align='center' style='border:1px solid black; font-family:verdana'>";
		msg += "<h3 style='color: blue;'>";
		msg += userID + "님의 임시 비밀번호 입니다. 비밀번호를 변경하여 사용하세요.</h3>";
		msg += "<p>임시 비밀번호 : " + newpw;


		// 받는 사람 E-Mail 주소
		String mail = userMail;
		try {
			HtmlEmail email = new HtmlEmail();
			email.setDebug(true);
			email.setCharset(charSet);
			email.setSSL(true);
			email.setHostName(hostSMTP);
			email.setSmtpPort(587);

			email.setAuthentication(hostSMTPid, hostSMTPpwd);
			email.setTLS(true);
			email.addTo(mail, charSet);
			email.setFrom(fromEmail, fromName, charSet);
			email.setSubject(subject);
			email.setHtmlMsg(msg);
			email.send();
		} catch (Exception e) {
			System.out.println("메일발송 실패 : " + e);
		}

	}
	//비밀번호 암호화
	public static String encrypt(String newpw) {
		try {
			MessageDigest md = MessageDigest.getInstance("SHA-256");
			md.update(newpw.getBytes());
			byte byteData[] = md.digest();
			StringBuffer sb = new StringBuffer();
			for (int i = 0; i < byteData.length; i++) {
				sb.append(Integer.toString((byteData[i] & 0xff) + 0x100, 16).substring(1));
			}
			StringBuffer hexString = new StringBuffer();
			for (int i = 0; i < byteData.length; i++) {
				String hex = Integer.toHexString(0xff & byteData[i]);
				if (hex.length() == 1) {
					hexString.append('0');
				}
				hexString.append(hex);
			}
			System.out.println(hexString.toString());
			hexString.toString();
			
			return  hexString.toString();
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException();
		}
	}
	//비밀번호 찾기 시 비밀번호 초기화 암호
	public static String createKey() throws Exception {

		StringBuffer key = new StringBuffer();
		Random rnd = new Random();

		for (int i = 0; i < 10; i++) {
			int index = rnd.nextInt(3);

			switch (index) {
			case 0:
				key.append((char) ((int) (rnd.nextInt(26)) + 97));
				break;
			case 1:
				key.append((char) ((int) (rnd.nextInt(26)) + 65));
				break;
			case 2:
				key.append((rnd.nextInt(10)));
				break;
			}
		}
		return key.toString();

	}

}
