package tproject.project.controller;

import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequestMapping("index")
public class homeController {
	@RequestMapping("index.do")
	public String index(HttpSession session) {
		return "index";
	}
}
