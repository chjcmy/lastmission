package tproject.project.service.logic;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import tproject.project.AWSS3.AWSService;
import tproject.project.domain.CafeInfo;
import tproject.project.service.CafeInfoService;
import tproject.project.store.CafeInfoStore;

@Service
public class CafeInfoServiceLogic implements CafeInfoService{

	
	@Autowired
	private CafeInfoStore cafeStore;
	
	AWSService s3 = new AWSService();
	
	@Override
	public void registerStudy(CafeInfo cafeInfo, MultipartFile file, List<MultipartFile> files) {
		// TODO Auto-generated method stub
		try {
			String imgurl = s3.fileUpload("webimage", file.getOriginalFilename(), file.getBytes());
			cafeInfo.setCafePicture1(imgurl);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
		int idx = 0;
		for (MultipartFile f : files) {
			try {
				String imgurl = s3.fileUpload("webimage", f.getOriginalFilename(), f.getBytes());
				if (idx == 0)
					cafeInfo.setCafePicture2(imgurl);
				else
					cafeInfo.setCafePicture3(imgurl);
			} catch (Exception e) {
				throw new RuntimeException(e);
			}
			idx++;
		}
		cafeStore.CreateStudy(cafeInfo);
	}
	
	@Override
	public CafeInfo findContent_detail(int cafeId) {
		// TODO Auto-generated method stub
		CafeInfo cafeInfo = cafeStore.FindStudyDetail(cafeId);
		try {
			String imgURL = s3.getFileURL(cafeInfo.getCafePicture2());
			cafeInfo.setCafePicture2(imgURL);
			imgURL = s3.getFileURL(cafeInfo.getCafePicture1());
			cafeInfo.setCafePicture1(imgURL);
			imgURL = s3.getFileURL(cafeInfo.getCafePicture3());
			cafeInfo.setCafePicture3(imgURL);
		} catch (Exception e) {
			System.out.println(e);
		}
		return cafeInfo;
	}
	
	@Override
	public List<CafeInfo> findMyContent(String userId) {
		// TODO Auto-generated method stub
		return cafeStore.FindMyStudy(userId);
	}

	@Override
	public void removeContent(int cafeId) {
		// TODO Auto-generated method stub
		cafeStore.DeleteStudy(cafeId);
	}

	@Override
	public void modifyContent(CafeInfo cafeInfo) {
		// TODO Auto-generated method stub
		cafeStore.UpdateStudy(cafeInfo);
	}
	@Override
	public List<CafeInfo> findContent(String location, int pagenum, int contentnum) {
		// TODO Auto-generated method stub
		List<CafeInfo> cafeList=cafeStore.FindStudy(location, pagenum, contentnum);
		for (CafeInfo cafeInfo : cafeList) {
			String imgURL = s3.getFileURL(cafeInfo.getCafePicture2());
			cafeInfo.setCafePicture2(imgURL);
		}
		return cafeList;
	}

	@Override
	public int totalcount(String location) {
		// TODO Auto-generated method stub
		return cafeStore.totalcount(location);
	}


}
