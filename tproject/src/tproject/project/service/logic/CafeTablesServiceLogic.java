package tproject.project.service.logic;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import tproject.project.domain.CafeTable;
import tproject.project.service.CafeTableService;
import tproject.project.store.CafeInfoStore;

@Service
public class CafeTablesServiceLogic implements CafeTableService{

	@Autowired
	CafeInfoStore cafeStore;
	@Override
	public void regist(CafeTable cafeTable) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public List<CafeTable> groupTable(List<String> tables, int cafeId) {
		List<CafeTable> cafeTables=new ArrayList<CafeTable>();
		for(int i=1;i<=tables.size();i++) {
			CafeTable table =new CafeTable();
			table.setTableNum(String.valueOf(i));
			table.setStudyId(String.valueOf(cafeId));
			table.setTableSize(tables.get(i-1));
			cafeTables.add(table);
		}
		return cafeTables;
	}

	@Override
	public List<CafeTable> findAllTables(int cafeId) {
		// TODO Auto-generated method stub
		
		return cafeStore.retieveAllTables(cafeId);
	}



}
