package tproject.project.service.logic;



import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import tproject.project.domain.User;
import tproject.project.domain.UserTable;
import tproject.project.service.UserService;
import tproject.project.store.UserStore;

@Service
public class UserServiceLogic implements UserService {
	@Autowired
	private UserStore store;

	@Override
	public void insertUser(User user) {
		// TODO Auto-generated method stub

		store.insertUser(user);

	}

	@Override
	public User loginUser(String userID, String userPW) {
		// TODO Auto-generated method stub

		return store.loginUser(userID, userPW);
	}

	@Override
	public void updateUser(User user) {
		// TODO Auto-generated method stub
		store.updateUser(user);
	}

	@Override
	public void deleteUser(String userID, String userPW) throws Exception {
		// TODO Auto-generated method stub
		store.deleteUser(userID, userPW);
	}

	@Override
	public User findid(String userName, String userMail) throws Exception {
		// TODO Auto-generated method stub
		return store.findid(userName, userMail);

	}

	@Override
	public List<UserTable> userlist(String userID) {
		// TODO Auto-generated method stub
		return store.userlist(userID);
	}

	@Override
	public List<UserTable> cafelist(String userID) {
		// TODO Auto-generated method stub
		return store.cafelist(userID);
	}

	@Override
	public User findpw(String userID, String userName, String userMail) {
		// TODO Auto-generated method stub
		return store.findpw(userID, userName, userMail);
	}

	@Override
	public void changepw(String userID, String newpw) {
		// TODO Auto-generated method stub
		store.changepw(userID, newpw);
	}




	



	

}
