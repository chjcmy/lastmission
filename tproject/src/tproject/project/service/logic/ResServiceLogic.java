package tproject.project.service.logic;

import java.sql.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import tproject.project.AWSS3.AWSService;
import tproject.project.domain.ResInfo;
import tproject.project.service.ResService;
import tproject.project.store.ResStore;

@Service
public class ResServiceLogic implements ResService {
	
	AWSService s3 = new AWSService();
	
	@Autowired
	private ResStore resStore;
	@Override
	public List<ResInfo> showRes(Date resDate, int StudyId,String tableNum) {
		
		return resStore.findAllRes(resDate, StudyId,tableNum);
	}
	@Override
	public void registRes(ResInfo resInfo,MultipartFile file) {
		// TODO Auto-generated method stub
		try {
			String fileurl=s3.fileUpload("docu",file.getOriginalFilename() ,file.getBytes());
			resInfo.setFileL(fileurl);
		} catch (Exception e) {
			// TODO: handle exception
		}
		resStore.createRes(resInfo);
	}
	@Override
	public void registRes(ResInfo resInfo) {
		// TODO Auto-generated method stub
		resStore.createRes(resInfo);
	}

}
