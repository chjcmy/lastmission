package tproject.project.service;

import java.util.List;

import org.springframework.web.multipart.MultipartFile;

import tproject.project.domain.CafeInfo;
public interface CafeInfoService  {

	void registerStudy(CafeInfo cafeInfo,MultipartFile file,List<MultipartFile> files);

	List<CafeInfo> findContent(String location, int pagenum, int contentnum);

	CafeInfo findContent_detail(int cafeId);
	
	List<CafeInfo> findMyContent(String userId);
	
	void removeContent(int cafeId);

	void modifyContent(CafeInfo cafeInfo);

	int totalcount(String location);
}
