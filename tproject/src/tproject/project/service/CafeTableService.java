package tproject.project.service;

import java.util.List;

import tproject.project.domain.CafeTable;

public interface CafeTableService {
	void regist(CafeTable cafeTable);
	
	List<CafeTable> groupTable(List<String> tables,int cafeId);
	
	List<CafeTable> findAllTables(int cafeId);
}
