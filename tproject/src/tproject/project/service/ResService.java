package tproject.project.service;

import java.sql.Date;
import java.util.List;

import org.springframework.web.multipart.MultipartFile;

import tproject.project.domain.ResInfo;

public interface ResService {

	List<ResInfo> showRes(Date resDate, int StudyId,String tableNum);
	
	void registRes(ResInfo resInfo,MultipartFile file);
	void registRes(ResInfo resInfo);
}
