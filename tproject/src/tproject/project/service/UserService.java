package tproject.project.service;

import java.util.List;


import tproject.project.domain.User;
import tproject.project.domain.UserTable;

public interface UserService {

	public void insertUser(User user);

	public User loginUser(String userID, String userPW);

	public void updateUser(User user);

	public void deleteUser(String userID, String userPW) throws Exception;

	public User findid(String userName, String userMail) throws Exception;

	public List<UserTable> userlist(String userID);

	public List<UserTable> cafelist(String userID);

	public User findpw(String userID, String userName, String userMail);

	public void changepw(String userID, String newpw);

}
