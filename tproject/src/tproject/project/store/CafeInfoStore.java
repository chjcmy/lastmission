package tproject.project.store;

import java.util.List;

import tproject.project.domain.CafeInfo;
import tproject.project.domain.CafeTable;

public interface CafeInfoStore {

	// �뒪�꽣�뵒 移댄럹 �깮�꽦
	void CreateStudy(CafeInfo cafeinfo);
	// insert 臾몄쓣 �씠�슜�븯湲� �븣臾몄뿉 紐⑤뱺 �븘�뱶媛� �븘�슂�븯�떎.
	// �뵲�씪�꽌 ,媛앹껜瑜� 留ㅺ컻蹂��닔濡� 諛쏆븘�빞 �븿
	// �빐�떦 硫붿냼�뱶�쓽 �뿭�븷�� 留뚮뱶�뒗 寃� 源뚯��씠湲� �븣臾몄뿉 void

	// �뒪�꽣�뵒 移댄럹 議고쉶

	// 硫붿씤 議고쉶 �럹�씠吏� (怨듯넻)
	List<CafeInfo> FindStudy(String location, int pagenum, int contentnum);

	// �긽�꽭 議고쉶 �럹�씠吏�
	CafeInfo FindStudyDetail(int id);

	// �벑濡앺븳 移댄럹 議고쉶(移댄럹 二쇱씤)
	List<CafeInfo> FindMyStudy(String userId);

	void DeleteStudy(int studyId);

	void UpdateStudy(CafeInfo cafeinfo);

	List<CafeTable> retieveAllTables(int cafeId);

	int totalcount(String location);

}
