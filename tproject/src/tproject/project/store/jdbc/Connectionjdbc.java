package tproject.project.store.jdbc;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class Connectionjdbc {
	private static Connectionjdbc instance;

	private static final String DRIVER_NAME = "com.mysql.jdbc.Driver";
	private static final String URL = "jdbc:mysql://34.66.87.65:3306/sys";
	private static final String USER_NAME = "team";
	private static final String PASSWORD = "1234";

	private Connectionjdbc() {
		try {
			Class.forName(DRIVER_NAME);
		} catch (ClassNotFoundException e) {
			throw new RuntimeException(e);
		}
	}

	public static Connectionjdbc getInstance() {
		if (instance == null) {
			instance = new Connectionjdbc();
		}
		return instance;
	}

	public Connection createConnection() throws SQLException {
		System.out.println("DB연결 성공!");
		return DriverManager.getConnection(URL, USER_NAME, PASSWORD);
	}
}