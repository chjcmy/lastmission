package tproject.project.store;

import java.sql.Date;
import java.util.List;

import tproject.project.domain.ResInfo;

public interface ResStore {
	List<ResInfo> findAllRes(Date resDate,int StudyId,String tableNum);
	
	void createRes(ResInfo resInfo);
}
