package tproject.project.store.logic;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;

import tproject.project.domain.User;
import tproject.project.domain.UserTable;
import tproject.project.store.UserStore;
import tproject.project.store.jdbc.Connectionjdbc;
import tproject.project.store.jdbc.JdbcUtils;

@Repository
public class UserStoreLogic implements UserStore {
	private Connectionjdbc db;

	public UserStoreLogic() {
		db = Connectionjdbc.getInstance();
		System.out.println("스토어측: 서버 연결 성공!!");
	}

	@Override
	public void insertUser(User user) {
		// TODO Auto-generated method stub

		System.out.println("Uid: " + user.getUserID());
		System.out.println("Upw: " + user.getUserPW());
		System.out.println("Uname: " + user.getUserName());
		System.out.println("Utel: " + user.getUserTel());
		System.out.println("Uemail: " + user.getUserMail());
		System.out.println("Uage: " + user.getUserAge());
		System.out.println("Ujob: " + user.getUserJob());
		System.out.println("Ugrade: " + user.getUserGrade());

		String sql = "INSERT INTO user(Uid, Upass, Uname, Uphone, Uemail, Uage, Ujob, Ugrade,Uact)"
				+ "VALUES (?,?,?,?,?,?,?,?,0)";

		Connection conn = null;
		PreparedStatement psmt = null;

		try {
			conn = db.createConnection();
			psmt = conn.prepareStatement(sql.toString());
			psmt.setString(1, user.getUserID());
			psmt.setString(2, user.getUserPW());
			psmt.setString(3, user.getUserName());
			psmt.setString(4, user.getUserTel());
			psmt.setString(5, user.getUserMail());
			psmt.setString(6, user.getUserAge());
			psmt.setString(7, user.getUserJob());
			psmt.setString(8, user.getUserGrade());
			psmt.executeUpdate();

			System.out.println("스토어측: db 계정삽입 완료!!");

		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			JdbcUtils.close(psmt, conn);
		}

	}

	@Override
	public User loginUser(String userID, String userPW) {
		// TODO Auto-generated method stub

		User user = new User();

		Connection conn = null;
		PreparedStatement psmt = null;
		ResultSet rs = null;
		// String sql = "SELECT PASSWORD_ID, EMAIL_ID, DATE_CREATE, POINT_ID, USER_NAME,
		// NICK_NAME, TEL_ID, ID_NUMBER FROM USERTB WHERE ID_ID=?";
		String sql = "SELECT * FROM user WHERE Uid= ? and Uact= 0 ";
		try {

			conn = db.createConnection();
			psmt = conn.prepareStatement(sql);
			psmt.setString(1, userID);
			rs = psmt.executeQuery();

			if (rs.next()) {
				if (userPW.equals(rs.getString("Upass"))) {
					user.setUserName(rs.getString("Uname"));
					user.setUserTel(rs.getString("Uphone"));
					user.setUserMail(rs.getString("Uemail"));
					user.setUserAge(rs.getString("Uage"));
					user.setUserJob(rs.getString("Ujob"));
					user.setUserGrade(rs.getString("Ugrade"));
					user.setLogined(true);
				} else {
					System.out.println("DB오류");
				}
			} else {
				System.out.println("로그인정보 없음!");
			}
		} catch (SQLException e) {
			System.out.println("로그인 오류발생!");
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		finally {
			System.out.println("로그인 성공!!");
			JdbcUtils.close(psmt, conn);

		}

		return user;
	}

	@Override
	public void updateUser(User user) {
		// TODO Auto-generated method stub
		String sql = "UPDATE user SET Upass=?, Uname=?, Uphone=?, Uemail=?, Uage=?, Ujob=?, Ugrade=?" + " WHERE Uid=?";

		Connection conn = null;
		PreparedStatement psmt = null;

		try {
			conn = db.createConnection();
			psmt = conn.prepareStatement(sql.toString());
			psmt.setString(1, user.getUserPW());
			psmt.setString(2, user.getUserName());
			psmt.setString(3, user.getUserTel());
			psmt.setString(4, user.getUserMail());
			psmt.setString(5, user.getUserAge());
			psmt.setString(6, user.getUserJob());
			psmt.setString(7, user.getUserGrade());
			psmt.setString(8, user.getUserID());
			psmt.executeUpdate();

			System.out.println("스토어측: 계정 정보변경 완료!!");

		} catch (SQLException e) {
			System.out.println("스토어측: 계정 정보변경 중 오류 발생!!");
			throw new RuntimeException(e);
		} finally {
			JdbcUtils.close(psmt, conn);
		}
	}

	@Override
	public void deleteUser(String userID, String userPW) {
		// TODO Auto-generated method stub
		Connection conn = null;
		PreparedStatement psmt = null;

		// String sql = "DELETE FROM USERTB WHERE ID_ID=? AND PASSWORD_ID=?";
		String sql = "UPDATE user SET Uact=1 WHERE Uid=? AND Upass=?";

		try {
			conn = db.createConnection();
			psmt = conn.prepareStatement(sql);
			psmt.setString(1, userID);
			psmt.setString(2, userPW);
			psmt.executeUpdate();
			System.out.println("탈퇴 성공!!");
		}

		catch (SQLException e) {
			System.out.println("탈퇴 오류발생!");
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		finally {
			JdbcUtils.close(psmt, conn);
		}
	}

	@Override
	public User findid(String userName, String userMail) {
		// TODO Auto-generated method stub
		User user = new User();
		String id = null;

		Connection conn = null;
		PreparedStatement psmt = null;
		ResultSet rs = null;
		// String sql = "SELECT PASSWORD_ID, EMAIL_ID, DATE_CREATE, POINT_ID, USER_NAME,
		// NICK_NAME, TEL_ID, ID_NUMBER FROM USERTB WHERE ID_ID=?";
		String sql = "SELECT Uid FROM user WHERE Uname= ? and Uemail= ? ";
		try {
			conn = db.createConnection();
			psmt = conn.prepareStatement(sql);
			System.out.println(userName);
			System.out.println(userMail);
			psmt.setString(1, userName);
			psmt.setString(2, userMail);
			rs = psmt.executeQuery();

			if (rs.next()) {

				System.out.println("스토어측: 맞는 id찾음!!");
				id = (rs.getString("Uid"));
				System.out.println(id);
				user.setUserID(rs.getString("Uid"));
			} else {
				System.out.println("DB오류");
			}
		} catch (SQLException e) {
			System.out.println("로그인 오류발생!");
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		finally {
			JdbcUtils.close(psmt, conn);
		}
		return user;
	}

	@Override
	public List<UserTable> userlist(String userID) {
		System.out.println(userID);
		String sql = "SELECT  study.Sname, reservation.Uid, reservation.Rtable, reservation.Rdate, reservation.Rstart, reservation.Rend, reservation.Rcount,reservation.Rprice, reservation.Rpurpose, reservation.Rprint from reservation,study where study.Sid = reservation.Sid and reservation.Uid = ?";
		Connection conn = null;
		PreparedStatement psmt = null;
		ResultSet rs = null;

		List<UserTable> userList = new ArrayList<UserTable>();

		try {
			conn = db.createConnection();
			psmt = conn.prepareStatement(sql.toString());
			psmt.setString(1, userID);
			rs = psmt.executeQuery();
			while (rs.next()) {
				UserTable usertable = new UserTable();
				usertable.setCafename(rs.getString("Sname"));
				usertable.setTablenum(rs.getString("Rtable"));
				usertable.setStartTime(rs.getString("Rstart"));
				usertable.setEndTime(rs.getString("Rend"));
				usertable.setrDate(rs.getString("Rdate"));
				usertable.setPersonNum(rs.getString("Rcount"));
				usertable.setPurpose(rs.getString("Rpurpose"));
				usertable.setFile(rs.getString("Rprint"));
				usertable.setPrice(rs.getString("Rprice"));
				userList.add(usertable);

			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			JdbcUtils.close(conn, rs, psmt);
		}
		return userList;
	}

	@Override
	public List<UserTable> cafelist(String userID) {
		System.out.println(userID);
		String sql = "SELECT Sid From study where Uid = ? ";
		Connection conn = null;
		PreparedStatement psmt = null;
		ResultSet rs = null;
		List<String> sidList = new ArrayList<String>();
		try {
			conn = db.createConnection();
			psmt = conn.prepareStatement(sql.toString());
			psmt.setString(1, userID);
			rs = psmt.executeQuery();
			while (rs.next()) {
				String s = rs.getString("Sid");
				sidList.add(s);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			JdbcUtils.close(conn, rs, psmt);
		}
		List<UserTable> cafeList = new ArrayList<UserTable>();
		for (String sid : sidList) {
			sql = "SELECT Uid, Rdate, Rstart, Rend, Rprice, Rcount, Rpurpose, Rprint, Rtable FROM reservation where reservation.Sid = ?";

			try {
				conn = db.createConnection();
				psmt = conn.prepareStatement(sql.toString());
				psmt.setString(1, sid);
				rs = psmt.executeQuery();
				while (rs.next()) {
					UserTable cafetable = new UserTable();
					cafetable.setUserid(rs.getString("Uid"));
					cafetable.setrDate(rs.getString("Rdate"));
					cafetable.setStartTime(rs.getString("Rstart"));
					cafetable.setEndTime(rs.getString("Rend"));
					cafetable.setPrice(rs.getString("Rprice"));
					cafetable.setPersonNum(rs.getString("Rcount"));
					cafetable.setFile(rs.getString("Rprint"));
					cafetable.setTablenum(rs.getString("Rtable"));
					cafeList.add(cafetable);
				}
			} catch (SQLException e) {
				e.printStackTrace();
			} finally {
				JdbcUtils.close(conn, rs, psmt);
			}
		}
		System.out.println(cafeList.size());
		return cafeList;

	}

	@Override
	public User findpw(String userID, String userName, String userMail) {

		User user = new User();

		System.out.println(userID);
		String sql = "SELECT Uact from user where Uid = ? and Uname = ? and Uemail = ?";
		Connection conn = null;
		PreparedStatement psmt = null;
		ResultSet rs = null;

		try {
			conn = db.createConnection();
			psmt = conn.prepareStatement(sql.toString());
			psmt.setString(1, userID);
			psmt.setString(2, userName);
			psmt.setString(3, userMail);
			rs = psmt.executeQuery();
			while (rs.next()) {
				user.setAct(rs.getString("Uact"));

			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			JdbcUtils.close(conn, rs, psmt);
		}
		return user;

	}

	@Override
	public void changepw(String userID, String newpw) {
		System.out.println(newpw);
		String sql = "UPDATE user SET Upass = ? WHERE Uid = ?";
		Connection conn = null;
		PreparedStatement psmt = null;

		try {
			conn = db.createConnection();
			psmt = conn.prepareStatement(sql.toString());
			psmt.setString(1, newpw);
			psmt.setString(2, userID);
			psmt.executeUpdate();

		} catch (SQLException e) {
			System.out.println("탈퇴 오류발생!");
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		finally {
			JdbcUtils.close(psmt, conn);
		}
	}
}
