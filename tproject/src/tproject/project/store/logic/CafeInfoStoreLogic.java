package tproject.project.store.logic;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;

import tproject.project.AWSS3.AWSService;
import tproject.project.domain.CafeInfo;
import tproject.project.domain.CafeTable;
import tproject.project.store.CafeInfoStore;
import tproject.project.store.jdbc.Connectionjdbc;
import tproject.project.store.jdbc.JdbcUtils;

@Repository
public class CafeInfoStoreLogic implements CafeInfoStore {
	private Connectionjdbc connectionFactory;

	public CafeInfoStoreLogic() {

		connectionFactory = Connectionjdbc.getInstance();
	}

	AWSService s3 = new AWSService();

	@Override
	public void CreateStudy(CafeInfo cafeInfo) {
		// 스터디 카페 생성

		String sql = "INSERT INTO study(Sname,Uid,Slocation,Sstart,Send,Sbody,Sfile_1,Sfile_2,Sfile_3,Sprice,Slat,Slong) VALUES (?,?,?,?,?,?,?,?,?,?,?,?)";
		String sql2 = "INSERT INTO room_info(Sid,Rtable,Room_count) VALUES(?,?,?)";
		Connection conn = null;
		PreparedStatement psmt = null;
		ResultSet rs = null;
		try {
			conn = connectionFactory.createConnection();
			psmt = conn.prepareStatement(sql.toString(), Statement.RETURN_GENERATED_KEYS);
			psmt.setString(1, cafeInfo.getCafeName());
			psmt.setString(2, cafeInfo.getUserId());
			psmt.setString(3, cafeInfo.getCafeLocation());
			psmt.setString(4, cafeInfo.getCafeStart());
			psmt.setString(5, cafeInfo.getCafeEnd());
			psmt.setString(6, cafeInfo.getCafeBody());
			psmt.setString(7, cafeInfo.getCafePicture1());
			psmt.setString(8, cafeInfo.getCafePicture2());
			psmt.setString(9, cafeInfo.getCafePicture3());
			psmt.setInt(10, cafeInfo.getCafePrice());
			psmt.setString(11, cafeInfo.getClat());
			psmt.setString(12, cafeInfo.getClong());
			psmt.executeUpdate();
			rs = psmt.getGeneratedKeys();
			int cafeId = 0;
			if (rs.next()) {
				cafeId = rs.getInt(1);
			}
			if (cafeId != 0) {
				List<CafeTable> tables = cafeInfo.getCafeTables();
				for (int i = 0; i < tables.size(); i++) {
					psmt = conn.prepareStatement(sql2.toString());
					psmt.setInt(1, cafeId);
					psmt.setString(2, tables.get(i).getTableNum());
					psmt.setString(3, tables.get(i).getTableSize());
					psmt.execute();
				}
			}
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			JdbcUtils.close(rs, psmt, conn);
		}
	}

	// 스터디 카페 조회
	// 메인 조회 페이지 (공통)
	@Override
	public List<CafeInfo> FindStudy(String location, int pagenum, int contentnum) {

		String sql = "select Sid,Sname,Sprice, Slocation, Sstart, Send, Sfile_1,Sfile_2,Sfile_3 from study where Slocation like ? limit ?,?";
		Connection conn = null;
		PreparedStatement psmt = null;
		ResultSet rs = null;

		List<CafeInfo> CafeList = new ArrayList<CafeInfo>();

		try {
			conn = connectionFactory.createConnection();
			psmt = conn.prepareStatement(sql.toString());
			psmt.setString(1,"%"+location+"%");// 주소값 변경해야됩니다.
			psmt.setInt(2, pagenum);
			psmt.setInt(3, contentnum);
			String sas = psmt.toString();
			rs = psmt.executeQuery();
			while (rs.next()) {
				CafeInfo cafeinfo = new CafeInfo();
				cafeinfo.setCafeId(rs.getInt("Sid"));
				cafeinfo.setCafeName(rs.getString("Sname"));
				cafeinfo.setCafePrice(rs.getInt("Sprice"));
				cafeinfo.setCafeLocation(rs.getString("Slocation"));
				cafeinfo.setCafeStart(rs.getString("Sstart"));
				cafeinfo.setCafeEnd(rs.getString("Send"));
				cafeinfo.setCafePicture1(rs.getString("Sfile_1"));
				cafeinfo.setCafePicture2(rs.getString("Sfile_2"));
				cafeinfo.setCafePicture3(rs.getString("Sfile_3"));
				CafeList.add(cafeinfo);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			JdbcUtils.close(conn, rs, psmt);
		}
		return CafeList;
	}

	// 상세 조회 페이지
	public CafeInfo FindStudyDetail(int cafeId) {
		String sql = "select Sname,Slocation,Sstart,Send,Sbody,Sfile_1,Sfile_2,Sfile_3,Sprice,Slat,Slong" + " from study "
				+ "where Sid= ?";

		Connection conn = null;
		PreparedStatement psmt = null;
		ResultSet rs = null;

		CafeInfo cafeinfo = null;
		try {
			conn = connectionFactory.createConnection();
			psmt = conn.prepareStatement(sql.toString());
			psmt.setInt(1, cafeId);
			rs = psmt.executeQuery();
			if (rs.next()) {
				cafeinfo = new CafeInfo();
				cafeinfo.setCafeId(cafeId);
				cafeinfo.setCafeName(rs.getString("Sname"));
				cafeinfo.setCafeLocation(rs.getString("Slocation"));
				cafeinfo.setCafeStart(rs.getString("Sstart"));
				cafeinfo.setCafeEnd(rs.getString("Send"));
				cafeinfo.setCafeBody(rs.getString("Sbody"));
				cafeinfo.setCafePicture1(rs.getString("Sfile_1"));
				cafeinfo.setCafePicture2(rs.getString("Sfile_2"));
				cafeinfo.setCafePicture3(rs.getString("Sfile_3"));
				cafeinfo.setCafePrice(rs.getInt("Sprice"));
				cafeinfo.setClat(rs.getString("Slat"));
				cafeinfo.setClong(rs.getString("Slong"));
			}
			System.out.println("suc");
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			JdbcUtils.close(psmt, conn, rs);
		}
		return cafeinfo;
	}

	// 등록한 카페 조회(카페 주인)
	public List<CafeInfo> FindMyStudy(String userId) {
		String sql = " select * " + "from study" + "where Uid like ?";

		Connection conn = null;
		PreparedStatement psmt = null;
		ResultSet rs = null;

		List<CafeInfo> CafeList = new ArrayList<CafeInfo>();

		try {
			conn = connectionFactory.createConnection();
			psmt = conn.prepareStatement(sql.toString());
			// psmt.setString(1, id);
			rs = psmt.executeQuery();
			while (rs.next()) {
				CafeInfo cafeinfo = new CafeInfo();
				cafeinfo.setCafeId(rs.getInt("Sid"));
				cafeinfo.setCafeName(rs.getString("Sname"));
				cafeinfo.setCafeLocation(rs.getString("Slocation"));
				cafeinfo.setCafeStart(rs.getString("Sstart"));
				cafeinfo.setCafeEnd(rs.getString("Send"));
				cafeinfo.setCafeBody(rs.getString("Sbody"));
				cafeinfo.setCafePicture1(rs.getString("Sfile_1"));
				cafeinfo.setCafePicture2(rs.getString("Sfile_2"));
				cafeinfo.setCafePicture3(rs.getString("Sfile_3"));
				cafeinfo.setCafePrice(rs.getInt("Sprice"));
				CafeList.add(cafeinfo);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			JdbcUtils.close(conn, rs, psmt);
		}
		return CafeList;
	}

	// 스터디 카페 수정
	@Override
	public void UpdateStudy(CafeInfo cafeinfo) {

		// 스터디 카페 삭제
	}

	@Override
	public void DeleteStudy(int studyId) {

	}

	@Override
	public List<CafeTable> retieveAllTables(int cafeId) {
		String sql = "select Room_id,Sid,Rtable,Room_count " + "from room_info " + "where Sid = ?";

		Connection conn = null;
		PreparedStatement psmt = null;
		ResultSet rs = null;

		List<CafeTable> cafeTables = new ArrayList<CafeTable>();

		try {
			conn = connectionFactory.createConnection();
			psmt = conn.prepareStatement(sql.toString());
			psmt.setInt(1, cafeId);
			rs = psmt.executeQuery();
			while (rs.next()) {
				CafeTable cafeTable = new CafeTable();
				cafeTable.setTableId(rs.getString("Room_id"));
				cafeTable.setStudyId(rs.getString("Sid"));
				cafeTable.setTableNum(rs.getString("Rtable"));
				cafeTable.setTableSize(rs.getString("Room_count"));
				cafeTables.add(cafeTable);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			JdbcUtils.close(conn, rs, psmt);
		}
		return cafeTables;
	}

	@Override
	public int totalcount(String location) {
		String sql = "select count(*) from study where Slocation like ?";

		Connection conn = null;
		PreparedStatement psmt = null;
		ResultSet rs = null;
		int total = 0;

		try {
			conn = connectionFactory.createConnection();
			psmt = conn.prepareStatement(sql.toString());
			psmt.setString(1, location);
			rs = psmt.executeQuery();
			while (rs.next()) {
				total = (rs.getInt("count(*)"));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			JdbcUtils.close(conn, rs, psmt);
		}
		return total;
	}

}
