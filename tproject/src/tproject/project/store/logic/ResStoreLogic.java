package tproject.project.store.logic;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;

import tproject.project.domain.ResInfo;
import tproject.project.store.ResStore;
import tproject.project.store.jdbc.Connectionjdbc;
import tproject.project.store.jdbc.JdbcUtils;

@Repository
public class ResStoreLogic implements ResStore{
	private Connectionjdbc connectionFactory;

	public ResStoreLogic() {

		connectionFactory = Connectionjdbc.getInstance();
	}

	@Override
	public List<ResInfo> findAllRes(Date resDate, int StudyId,String tableNum) {
		// TODO Auto-generated method stub
		String sql="SELECT Sid,Rtable,Rdate,Rstart,Rend "+"FROM reservation "+"WHERE Rdate =? AND Sid = ? AND Rtable = ?";
		Connection conn =null;
		PreparedStatement psmt=null;
		ResultSet rs =null;
		
		List<ResInfo> ResList= new ArrayList<ResInfo>();
		try {
			conn= connectionFactory.createConnection();
			psmt = conn.prepareStatement(sql.toString());
			psmt.setDate(1, resDate);
			psmt.setInt(2, StudyId);
			psmt.setString(3, tableNum);
			rs=psmt.executeQuery();
			while(rs.next()) {
				ResInfo resinfo= new ResInfo();
				resinfo.setCafeId(rs.getInt("Sid"));
				resinfo.setTableNum(rs.getString("Rtable"));
				resinfo.setResDate(rs.getDate("Rdate"));
				resinfo.setStartT(rs.getInt("Rstart"));
				resinfo.setEndT(rs.getInt("Rend"));
				ResList.add(resinfo);
			}
		}catch (SQLException e) {
			e.printStackTrace();
		} finally {
			JdbcUtils.close(conn, rs, psmt);
		}
		return ResList;
	}

	@Override
	public void createRes(ResInfo resInfo) {
		// TODO Auto-generated method stub

		String sql = "INSERT INTO reservation(Sid, Uid, Rtable, Rdate, Rstart, Rend, Rprice,Rcount,Rprint)"
				+ "VALUES (?,?,?,?,?,?,?,?,?)";

		Connection conn = null;
		PreparedStatement psmt = null;

		try {
			conn = connectionFactory.createConnection();
			psmt = conn.prepareStatement(sql.toString());
			psmt.setInt(1, resInfo.getCafeId());
			psmt.setString(2, resInfo.getUserId());
			psmt.setString(3, resInfo.getTableNum());
			psmt.setDate(4, resInfo.getResDate());
			psmt.setInt(5, resInfo.getStartT());
			psmt.setInt(6, resInfo.getEndT());
			psmt.setString(7,resInfo.getResCost());
			psmt.setString(8,resInfo.getResCount());
			psmt.setString(9,resInfo.getFileL());
			psmt.executeUpdate();


		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			JdbcUtils.close(psmt, conn);
		}
	}

}
