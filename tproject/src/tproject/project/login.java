package tproject.project;

public class login {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		 String s  = "가나다라똠방각하ABCDE 가나다라똠방각하ABCDE"; // 원본 문자열
		    String s2;

		    System.out.println("원본:    " + s);
		    System.out.println(); // 줄바꿈


		    // 문자열에서 모든 똠방각하 다 지우기
		    s2 = s.replace("똠방각하", "");
		    System.out.println("삭제(1): " + s2); // 출력 결과: 가나다라ABCDE 가나다라ABCDE


		    // 첫번째 "똠방각하"만 없애기
		    s2 = s.replaceFirst("똠방각하", "");
		    System.out.println("삭제(2): " + s2); // 출력 결과: 가나다라ABCDE 가나다라똠방각하ABCDE


		    // 문자열에서 모든 똠방각하 다 지우기
		    s2 = s.replaceAll("똠방각하", "");
		    System.out.println("삭제(3): " + s2); // 출력 결과: 가나다라ABCDE 가나다라ABCDE
	}

}
